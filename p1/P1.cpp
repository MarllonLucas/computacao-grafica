#include "P1.h"
#include <iostream>
#include <string>

namespace cg
{ // begin namespace cg

inline Primitive*
makeBoxMesh()
{
  const vec4f p1{-0.5, -0.5, -0.5, 1};
  const vec4f p2{+0.5, -0.5, -0.5, 1};
  const vec4f p3{+0.5, +0.5, -0.5, 1};
  const vec4f p4{-0.5, +0.5, -0.5, 1};
  const vec4f p5{-0.5, -0.5, +0.5, 1};
  const vec4f p6{+0.5, -0.5, +0.5, 1};
  const vec4f p7{+0.5, +0.5, +0.5, 1};
  const vec4f p8{-0.5, +0.5, +0.5, 1};
  const Color c1{Color::black};
  const Color c2{Color::red};
  const Color c3{Color::yellow};
  const Color c4{Color::green};
  const Color c5{Color::blue};
  const Color c6{Color::magenta};
  const Color c7{Color::cyan};
  const Color c8{Color::white};

  // Box vertices
  static const vec4f v[]
  {
    p1, p5, p8, p4, // x = -0.5
    p2, p3, p7, p6, // x = +0.5
    p1, p2, p6, p5, // y = -0.5
    p4, p8, p7, p3, // y = +0.5
    p1, p4, p3, p2, // z = -0.5
    p5, p6, p7, p8  // z = +0.5
  };

  // Box vertex colors
  static const Color c[]
  {
    c1, c5, c8, c4, // x = -0.5
    c2, c3, c7, c6, // x = +0.5
    c1, c2, c6, c5, // y = -0.5
    c4, c8, c7, c3, // y = +0.5
    c1, c4, c3, c2, // z = -0.5
    c5, c6, c7, c8  // z = +0.5
  };

  // Box triangles
  static const GLMeshArray::Triangle t[]
  {
    { 0,  1,  2}, { 2,  3,  0},
    { 4,  5,  7}, { 5,  6,  7},
    { 8,  9, 11}, { 9, 10, 11},
    {12, 13, 14}, {14, 15, 12},
    {16, 17, 19}, {17, 18, 19},
    {20, 21, 22}, {22, 23, 20}
  };

  return new Primitive{new GLMeshArray{24, {v, 0}, {c, 1}, 12, t}};
}

} // end namespace cg

inline void
P1::buildScene()
{
  _current = _scene = new Scene{"Scene 1"};

  auto box = createNode(_scene->getRoot(), true);

  auto emptyObj1 = createNode(_scene->getRoot(), false);
  auto emptyObj2 = createNode(_scene->getRoot(), false);
  auto emptyObj3 = createNode(_scene->getRoot(), false);
  
  auto box2 = createNode(emptyObj1, true);
  auto box3 = createNode(emptyObj2, false);
  auto box4 = createNode(emptyObj3, true);
  auto box5 = createNode(box4, false);
  
}

// Metodo que ira criar dinamicamente os meus n�s nas minhas cenas
Reference<SceneObject> 
P1::createNode(Reference<SceneObject> parent, bool typeNode) {

  /*
    TypeNode:
      true: Box
      false: Empty Object
  */

  std::string name = typeNode ? "Box " : "Object ";
  // Depois verificar se o objeto que eu quero instaciar como filho seja igual a minha cena passada
  auto parentScene = parent->scene();

  // Pega o nome do meu objeto e adiciona um valor inteiro para o nome 
  name = name + std::to_string(getIndexObjects());

  auto newObject = new SceneObject{ name.c_str(), parentScene };

  // Verifico se o parent passado � igual a raiz da minha sena
  if (parentScene->getRoot() == parent) newObject->setParent(nullptr);
  else                                  newObject->setParent(parent);

  // Caso o mesmo seja uma box criada
  // Devo adicionar um componente primitivo nele
  if (typeNode) {
    auto primitive = makeBoxMesh();
    newObject->add(Reference<Component>(primitive));
  }

  return newObject;

}

void
P1::initialize()
{
  Application::loadShaders(_program, "p1.vs", "p1.fs");
  buildScene();
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(1.0f, 1.0f);
  glLineWidth(2.0f);
  glEnable(GL_LINE_SMOOTH);
  _program.use();
}

namespace ImGui
{
  void ShowDemoWindow(bool*);
}

void 
P1::hierarchyWindowRecursive(Reference<SceneObject> node) {

  bool openNode = false;
  ImGuiTreeNodeFlags flag{ ImGuiTreeNodeFlags_OpenOnArrow };

  if (node != node->scene()->getRoot()) {
  
    // Verifica se eu estou em um n� intermediario
    if (!node->getObjectEmpty()) {
        openNode = ImGui::TreeNodeEx(node, _current == node ? flag | ImGuiTreeNodeFlags_Selected : flag, node->name());
    }
    else {
      flag |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
      ImGui::TreeNodeEx(node, _current == node ? flag | ImGuiTreeNodeFlags_Selected : flag, node->name());
    }

    if (ImGui::IsItemClicked()){
      _current = node;
    }
  
  }

  // Verifico se o meu no esta aberto na minha interface, ou se o mesmo � a minha raiz
  if (openNode || _scene->getRoot() == node) {

    for (auto it : node->getObjectsIterator())
      hierarchyWindowRecursive(it);
    
    ImGui::TreePop();

  }

}

inline void
P1::removeObject(Reference<SceneObject> object) {
  auto father = object->parent();
  father->remove(object);

  if (father == father->scene()->getRoot())
    _current = father->scene();
  else
    _current = father;

}

inline void
P1::hierarchyWindow()
{
  ImGui::Begin("Hierarchy");

  if (ImGui::Button("Create###object"))
    ImGui::OpenPopup("CreateObjectPopup");
  if (ImGui::BeginPopup("CreateObjectPopup"))
  {
    auto x = ImGui::MenuItem("Empty Object");
    
    if (x) {

      // pega um ponteiro do tipo SceneObject
      auto emptyObj = dynamic_cast<SceneObject*>(_current);
      // Cria um n� do tipo vazio
      // Verifico se o no atual � uma cena, para tratar o caso em que o ponteiro � null
      auto newEmpty = emptyObj ? createNode(emptyObj, false) : createNode(_scene->getRoot(), false);

    }

    if (ImGui::BeginMenu("3D Object"))
    {
      if (ImGui::MenuItem("Box"))
      {
        // pega um ponteiro do tipo SceneObject
        auto emptyObj = dynamic_cast<SceneObject*>(_current);
        // Cria um n� do tipo box
        auto newEmpty = emptyObj ? createNode(emptyObj, true) : createNode(_scene->getRoot(), true);

      }
      ImGui::EndMenu();
    }
    ImGui::EndPopup();
  }
  ImGui::SameLine();
  if (ImGui::Button("Delete"))
    ImGui::OpenPopup("Delete?");
  if (ImGui::BeginPopupModal("Delete?", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {

    auto box = dynamic_cast<SceneObject*>(_current);

    std::string texto;

    if (box)
      texto = "Tem certeza que deja apagar este objeto e todos os seu filhos?";
    else
      texto = "Ainda nao eh possivel fazer a remocao de uma cena!";

    ImGui::Text(texto.c_str());
    ImGui::Separator();


    if (box) {
      if (ImGui::Button("OK", ImVec2(120, 0))) {
        // Chamo a fun��o para a remo��o do objeto
        removeObject(box);
        // Removo a minha popup
        ImGui::CloseCurrentPopup();
      }
      ImGui::SetItemDefaultFocus();
      ImGui::SameLine();
    }


    if (ImGui::Button("Cancelar", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
    ImGui::EndPopup();
  }

  ImGui::Separator();

  ImGuiTreeNodeFlags flag{ImGuiTreeNodeFlags_OpenOnArrow};
  auto open = ImGui::TreeNodeEx(_scene, _current == _scene ? flag | ImGuiTreeNodeFlags_Selected : flag,_scene->name());

  if (ImGui::IsItemClicked())
    _current = _scene;
  
  if (open){

    // TODO: fazer uma verifica��o para uma cena
    hierarchyWindowRecursive(_scene->getRoot());
    
    
  }
  ImGui::End();
}

namespace ImGui
{ // begin namespace ImGui

void
ObjectNameInput(SceneNode* object)
{
  const int bufferSize{128};
  static SceneNode* current;
  static char buffer[bufferSize];

  if (object != current)
  {
    strcpy_s(buffer, bufferSize, object->name());
    current = object;
  }
  if (ImGui::InputText("Name", buffer, bufferSize))
    object->setName(buffer);
}

inline bool
ColorEdit3(const char* label, Color& color)
{
  return ImGui::ColorEdit3(label, (float*)&color);
}

inline bool
DragVec3(const char* label, vec3f& v)
{
  return DragFloat3(label, (float*)&v, 0.1f, 0.0f, 0.0f, "%.2g");
}

void
TransformEdit(Transform* transform)
{
  vec3f temp;

  temp = transform->localPosition();
  if (ImGui::DragVec3("Position", temp))
    transform->setLocalPosition(temp);
  temp = transform->localEulerAngles();
  if (ImGui::DragVec3("Rotation", temp))
    transform->setLocalEulerAngles(temp);
  temp = transform->localScale();
  if (ImGui::DragVec3("Scale", temp))
    transform->setLocalScale(temp);
}

} // end namespace ImGui

inline void
P1::sceneGui()
{
  auto scene = (Scene*)_current;

  ImGui::ObjectNameInput(_current);
  ImGui::Separator();
  if (ImGui::CollapsingHeader("Colors"))
  {
    ImGui::ColorEdit3("Background", backgroundColor);
    ImGui::ColorEdit3("Selected Wireframe", selectedWireframeColor);
  }
}

Reference<Transform> 
P1::isTransform(Reference<Component> component) {
  auto aux = dynamic_cast<Transform*>(component.get());

  // Caso o meu componente passado seja null, mando que o mesmo nao � do tipo transform
  return aux;
}

Reference<Primitive>
P1::isPrimitive(Reference<Component> component) {
  auto aux = dynamic_cast<Primitive*>(component.get());

  // Caso o meu componente passado seja null, mando que o mesmo nao � do tipo transform
  return aux;
}

inline void
P1::sceneObjectGui()
{
  auto object = (SceneObject*)_current;

  ImGui::ObjectNameInput(object);
  ImGui::SameLine();
  ImGui::Checkbox("visible", &object->visible);
  ImGui::Separator();

  // para cada componente do meu objeto eu mostro o mesmo na minha janela e come�o as minhas fun��es
  for (auto it : object->getComponentIterator()) {
    if (ImGui::CollapsingHeader(it->typeName())){
      
      // Retorna os ponteiros para os componentes criados em sua lista
      auto transf = isTransform(it);
      auto primit = isPrimitive(it);

      // Mostro as op��es de transform
      if (transf) {
        // Atualizo a minha matriz de transforma��o do meu objeto atual
        ImGui::TransformEdit(transf);
      }

      // Mostra as op��es de primitive
      if (primit) {

        // TODO: show primitive properties

      }


    }
  }
}

inline void
P1::objectGui()
{
  if (_current == nullptr)
    return;
  if (dynamic_cast<SceneObject*>(_current))
  {
    sceneObjectGui();
    return;
  }
  if (dynamic_cast<Scene*>(_current))
    sceneGui();
}

inline void
P1::inspectorWindow()
{
  ImGui::Begin("Inspector");
  objectGui();
  ImGui::End();
}

void
P1::gui()
{
  hierarchyWindow();
  inspectorWindow();
  
  /*static bool demo = true;
  ImGui::ShowDemoWindow(&demo);*/
  
}

// Metodo responsavel por fazer a renderiza��o de uma box especifica
void
P1::renderBox(Reference<Primitive> primitive) {
  // Verifico se a minha box esta visivel para renderiza��o
  auto box = primitive->sceneObject();

  if (!box->visible)
    return;
  
  _program.setUniformMat4("transform", box->transform()->localToWorldMatrix());
  // Caso o mesmo nao foi encontrado na minha lista de primitivos

  auto m = primitive->mesh();

  m->bind();
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glDrawElements(GL_TRIANGLES, m->vertexCount(), GL_UNSIGNED_INT, 0);
  if (_current != box)
    return;
  m->setVertexColor(selectedWireframeColor);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glDrawElements(GL_TRIANGLES, m->vertexCount(), GL_UNSIGNED_INT, 0);
  m->useVertexColors();

}

void
P1::render()
{
  GLWindow::render();

  // Para cada primitivo na minha cena eu devo renderiza-lo
  for (auto it : _scene->getIteratorPrimitives()) {
    renderBox(it);
  }
  
}
