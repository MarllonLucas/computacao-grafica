﻿Autores: Marlon Rodrigues e Pedro Flores

Atividades implementadas:

TODOS os itens listados na descrição do trabalho foram totalmente implementados:


A1 - Na atividade A1 como pedido, criamos um objeto de cena root (uma raiz) dentro do arquivo scene.h, este objeto root será responsável por armazenar todos os objetos da cena em sua lista de objetos. No arquivo SceneObject, foi realizado a criação de uma lista de SceneObject por meio da lib "list" do C++, este foi ultilizado pela facilidade de adição e remoção dos objetos por meio das funções dadas por ele. Cada objeto tem um referencia para o seu Pai, logo podemos acessas todos os objetos de cena por meio de uma busca de uma arvore.

A3 - No A3 criamos uma lista do tipo componentes, ao qual qualquer objeto que tem herda desta classe pode ser adicionado nesta lista, nesta implementação também ultilizamos uma lista encadiada da biblioteca "list" do c++.

A7 - Para que seja mostrado os objetos box criado na minha interface, foi criado uma lista de primitivos na minha classe "Scene", ao qual essa lista contém um referencia ao componente primitivo de um objeto da minha cena, isto foi adotado para que não precisa-se procurar em minha lista de objetos todos os objetos ao qual há um componente do tipo primitivo.

Todas as implementações de listas, foram implementadas utilizando uma biblioteca pronta do c++

Implementações adicionais: Foi implementado um botão de deletar um objeto da minha lista de hieraquia, ao qual, quando o objeto escolhido for apagado, a aplicação irá automaticamente apagar todos os filhos deste nó escolhido em castasta, até que o nó seja uma folha da minha arvore.