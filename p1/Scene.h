//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018 Orthrus Group.                               |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: Scene.h
// ========
// Class definition for scene.
//
// Author(s): Paulo Pagliosa and Marllon Rodrigues
// Last revision: 25/08/2018

#ifndef __Scene_h
#define __Scene_h

#include "SceneObject.h"
#include "graphics/Color.h"
#include <list>
#include "Primitive.h"

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// Scene: scene class
// =====
class Scene: public SceneNode
{
public:
  Color backgroundColor{Color::gray};

  /// Constructs an empty scene.
  Scene(const char* name):
    SceneNode{name}
  {
	// do nothing
	  // Inicializa a minha raiz
	  RootObject = new SceneObject("root", this);

  }

  // Meu meodo destrutor chama a minha limpeza de lista
  ~Scene() {
    clearList();
  }

  auto getRoot() 
  {
	  return RootObject;
  }

  // Retorna se minha lista de primitivos esta vazia
  bool isEmpty() {
    return primitives.empty();
  }

  auto size() {
    return primitives.size();
  }

  // Limpa a minha lista de primitivos
  void clearList() {
    primitives.clear();
  }

  // Adiciono o primitivo a minha lista de primitivos
  void add(Reference<Primitive> primitive) {
    primitives.push_back(primitive);
  }
  // Removo o meu primitivo da minha lista de primitivos
  void remove(Reference<Primitive> primitive) {
    primitives.remove(primitive);
  }

  auto getIteratorPrimitives() {
    return primitives;
  }

private:
	// Cria uma referencia para a raiz da minha arvore de objetos das cenas
	Reference<SceneObject> RootObject;
  // Cria uma lista de primitivos em minha cena
  // Eles s�o usados para fazer a renderiza��o das minhas box da cena
  std::list<Reference<Primitive>> primitives;

}; // Scene



} // end namespace cg

#endif // __Scene_h
