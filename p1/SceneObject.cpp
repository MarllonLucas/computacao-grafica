//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018 Orthrus Group.                               |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: SceneObject.cpp
// ========
// Source file for scene object.
//
// Author(s): Paulo Pagliosa and Marllon Rodrigues
// Last revision: 25/08/2018

#include "SceneObject.h"
#include "Scene.h"
#include <iostream>

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// SceneObject implementation
// ===========

SceneObject::~SceneObject() {

  auto scene = this->scene();

  for (auto it : this->components) {

    auto primitive = dynamic_cast<Primitive*>(it.get());

    if (primitive) {
      scene->remove(primitive);
    }

  }

  components.clear();
  objects_scene.clear();
}
void
SceneObject::setParent(SceneObject* parent)
{
	// pega o pai 
	auto father = this->parent();

	// Pega o no da minha raiz que est� sevindo como uma header para os meus objetos
	auto root = this->scene()->getRoot();

	/*
		passo 0: verificar se o meu pai � o meu root
			- Se for root: remover do root
			- Se nao: remover do pai
		passo 1: atualiza para o pai atual (_parent to parent)
		passo 2: adicionar na nova cole��o
	*/

	if (parent == nullptr) {

		// Verifica se existe um ponteiro no pai
		// caso exita eu removo esse objeto dessa lista em questao
		if (father != nullptr) {
			father->remove(this);
		}

		root->add(this);
		this->_parent = root;
         
	}
	else {

		if (father != nullptr) {
			father->remove(this);
		}

		parent->add(this);
		this->_parent = parent;

	}

}

// add object in the list
void 
SceneObject::add(Reference<SceneObject> object) {
  objects_scene.push_back(object);
}

void 
SceneObject::remove(Reference<SceneObject> object) {
  // Remove o objeto requisitado da minha lista de objetos
  
  for (auto it : object->getComponentIterator()) {

    auto primitive = dynamic_cast<Primitive*>(it.get());

    if (primitive) {
      _scene->remove(primitive);
    }

  }

  objects_scene.remove(object);
}

void
SceneObject::add(Reference<Component> component) {
  // Adicionar o componente na lista de componentes
  components.push_back(component);

  auto primitive = dynamic_cast<Primitive*>(component.get());

  if (primitive) {
    _scene->add(primitive);
    primitive->_sceneObject = this;
  }

}

void 
SceneObject::remove(Reference<Component> component) {
  // Remove o componente requisitado da minha lista de components
  

  auto primitive = dynamic_cast<Primitive*>(component.get());

  if (primitive) {
    _scene->remove(primitive);
  }

  components.remove(component);
}

} // end namespace cg
