#ifndef __P4_h
#define __P4_h

#include "Assets.h"
#include "BVH.h"
#include "GLRenderer.h"
#include "Light.h"
#include "Primitive.h"
#include "SceneEditor.h"
#include "RayTracer.h"
#include "core/Flags.h"
#include "graphics/Application.h"
#include "graphics/GLImage.h"

using namespace cg;

class P4: public GLWindow
{
public:
  P4(int width, int height):
    GLWindow{"cg2019 - P4", width, height},
    _program{"P4"}
  {
    // do nothing
    indexObjects = 1;
  }

  /// Initialize the app.
  void initialize() override;

  /// Update the GUI.
  void gui() override;

  /// Render the scene.
  void render() override;

  int getIndexObjects() {
    return indexObjects++;
  }

private:
  enum ViewMode
  {
    Editor = 0,
    Renderer = 1
  };

  enum class MoveBits
  {
    Left = 1,
    Right = 2,
    Forward = 4,
    Back = 8,
    Up = 16,
    Down = 32
  };

  enum class DragBits
  {
    Rotate = 1,
    Pan = 2
  };

  using BVHRef = Reference<BVH>;
  using BVHMap = std::map<TriangleMesh*, BVHRef>;

  GLSL::Program _program;
  GLSL::Program _programGouraud{ "Gouraud" };
  GLSL::Program _programPhong{ "Phong" };
  GLSL::Program _programFlat{ "Flat" };

  Reference<Scene> _scene;
  Reference<SceneEditor> _editor;
  Reference<GLRenderer> _renderer;


  SceneNode* _current{};
  Color _selectedWireframeColor{255, 102, 0};
  Flags<MoveBits> _moveFlags{};
  Flags<DragBits> _dragFlags{};
  int _pivotX;
  int _pivotY;
  int _mouseX;
  int _mouseY;
  bool _showAssets{true};
  bool _showEditorView{true};
  ViewMode _viewMode{ViewMode::Editor};
  Reference<RayTracer> _rayTracer;
  Reference<GLImage> _image;
  BVHMap bvhMap;

  static MeshMap _defaultMeshes;

  void buildScene();
  void renderScene();

  void mainMenu();
  void fileMenu();
  void showOptions();

  void hierarchyWindow();
  void inspectorWindow();
  void assetsWindow();
  void editorView();
  void sceneGui();
  void sceneObjectGui();
  void objectGui();
  void editorViewGui();
  void inspectPrimitive(Primitive&);
  void inspectShape(Primitive&);
  void inspectMaterial(Material&);
  void inspectLight(Light&);
  void inspectCamera(Camera&);
  void addComponentButton(SceneObject&);

  void drawPrimitive(Primitive&);
  void drawLight(Light&);
  void drawCamera(Camera&);

  bool windowResizeEvent(int, int) override;
  bool keyInputEvent(int, int, int) override;
  bool scrollEvent(double, double) override;
  bool mouseButtonInputEvent(int, int, int) override;
  bool mouseMoveEvent(double, double) override;

  Ray makeRay(int, int) const;

  static void buildDefaultMeshes();

  void hierarchyWindowRecursive(Reference<SceneObject> node);
  Reference<SceneObject>  createNode(Reference<SceneObject> parent, int typeNode, std::string shape);
  Reference<Camera>       createCamera(Reference<SceneObject> parent, std::string name);
  Reference<Light>        createLight(std::string name, int typeLight);

  void selectedLights();
  void startLightsAndCamera();

  inline void removeObject();

  void preview(Camera& camera);
  void drawPrimitives();
  void cameraFocus();

  inline void renderModeGui();
  void setScene(int indexScene);

  int indexObjects;
  Color _edgeColor{ 0,0,0 };
  bool _showEdges = true;

}; // P4

#endif // __P4_h
