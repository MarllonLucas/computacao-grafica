//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018 Orthrus Group.                               |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: Primitive.cpp
// ========
// Source file for primitive.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 30/10/2018

#include "Primitive.h"
#include "Transform.h"

namespace cg
{ // begin namespace cg

bool
Primitive::intersect(const Ray& ray, Intersection & hit) const
{
  if (_mesh == nullptr)
    return false;

  auto t = const_cast<Primitive*>(this)->transform();
  auto origem = t->worldToLocalMatrix().transform(ray.origin);
  auto direction = t->worldToLocalMatrix().transformVector(ray.direction);
  auto d = math::inverse(direction.length());
  Ray localRay{ origem, direction };
  direction = localRay.direction;
  origem = localRay.origin;

  float tMin;
  float tMax;

  if (_mesh->bounds().intersect(localRay, tMin, tMax))
  {

    bool ret = false;
    for (int i = 0; i < _mesh->data().numberOfTriangles; ++i) {
      auto aux = _mesh->data().triangles[i];

      auto P0 = _mesh->data().vertices[aux.v[0]];
      auto P1 = _mesh->data().vertices[aux.v[1]];
      auto P2 = _mesh->data().vertices[aux.v[2]];

      auto e1 = P1 - P0;
      auto e2 = P2 - P0;
      auto s1 = direction.cross(e2);
      auto s1e1 = s1.dot(e1);

      if (math::isZero(abs(s1e1))) {
        continue;
      }

      auto s = origem - P0;
      auto s2 = s.cross(e1);

      auto T = (s2.dot(e2)) * math::inverse(s1e1);

      if (!isgreaterequal(T, 0.0f)) 
        continue;

      auto dist = T * d;
      
      if (dist > hit.distance)
        continue;

      auto b1 = (s1.dot(s)) * math::inverse(s1e1);

      if (!isgreaterequal(b1, 0.0f)) {
        continue;
      }

      auto b2 = (s2.dot(direction)) * math::inverse(s1e1);

      if (!isgreaterequal(b2, 0.0f)) 
        continue;
      
      auto b1b2 = b1 + b2;

      if (isgreater(b1b2, 1))
        continue;
      
      hit.object = this;
      hit.triangleIndex = i;
      hit.distance = dist;
      hit.p = vec3f{ 1 - b1b2, b1, b2 };

      ret = true;

    }

    
    return ret;

  }
  return false;
}

} // end namespace cg
