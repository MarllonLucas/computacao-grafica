//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: Light.h
// ========
// Class definition for light.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 14/10/2019

#ifndef __Light_h
#define __Light_h

#include "Component.h"
#include "graphics/Color.h"

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// Light: light class
// =====
class Light: public Component
{
public:
  enum Type
  {
    Directional,
    Point,
    Spot
  };

  Color color{Color::white};

  Light(Type type) :
    Component{ "Light" },
    _type{ type },
    _falloff{ 0 },
    _angle{ 45.0 },
    _spotExponent{ 1 },
    _switched{ false }
  {
    // do nothing
  }

  auto type() const
  {
    return _type;
  }

  auto angle() {
    return _angle;
  }

  auto falloff() {
    return _falloff;
  }

  auto spotExponent() {
    return _spotExponent;
  }

  auto position() {
    return sceneObject()->transform()->position();
  }

  auto rotation() {
    return sceneObject()->transform()->rotation();
  }

  void setType(Type type){
    _type = type;
  }

  void setFalloff(int falloff) {
    _falloff = falloff;
  }

  void setAngle(float angle) {
    _angle = angle;
  }

  void setSpotExponent(int spotExponent) {
    _spotExponent = spotExponent;
  }

  void switchLight() {
    // Liga ou desliga a luz
    _switched = !_switched;
  }

  bool isTurnedOn() {
    return _switched;
  }

private:
  Type _type;
  vec4f _position;
  float _angle;
  int _falloff;
  bool _switched;
  int _spotExponent;

}; // Light

} // end namespace cg

#endif // __Light_h
