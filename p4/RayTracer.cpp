﻿//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: RayTracer.cpp
// ========
// Source file for simple ray tracer.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 16/10/2019

#include "Camera.h"
#include "RayTracer.h"
#include <time.h>

using namespace std;

namespace cg
{ // begin namespace cg

void
printElapsedTime(const char* s, clock_t time)
{
  printf("%sElapsed time: %.4f s\n", s, (float)time / CLOCKS_PER_SEC);
}


/////////////////////////////////////////////////////////////////////
//
// RayTracer implementation
// =========
RayTracer::RayTracer(Scene& scene, Camera* camera):
  Renderer{scene, camera},
  _maxRecursionLevel{6},
  _minWeight{MIN_WEIGHT}
{
  // TODO: BVH
}

void
RayTracer::render()
{
  throw std::runtime_error("RayTracer::render() invoked");
}

inline float
windowHeight(Camera* c)
{
  if (c->projectionType() == Camera::Parallel)
    return c->height();
  return c->nearPlane() * tan(math::toRadians(c->viewAngle() * 0.5f)) * 2;

}
void
RayTracer::renderImage(Image& image)
{
  auto t = clock();
  const auto& m = _camera->cameraToWorldMatrix();

  // VRC axes
  _vrc.u = m[0];
  _vrc.v = m[1];
  _vrc.n = m[2];
  // init auxiliary mapping variables
  _W = image.width();
  _H = image.height();
  _Iw = math::inverse(float(_W));
  _Ih = math::inverse(float(_H));

  auto height = windowHeight(_camera);

  _W >= _H ? _Vw = (_Vh = height) * _W * _Ih : _Vh = (_Vw = height) * _H * _Iw;
  // init pixel ray
  _pixelRay.origin = _camera->transform()->position();
  _pixelRay.direction = -_vrc.n;
  _camera->clippingPlanes(_pixelRay.tMin, _pixelRay.tMax);
  _numberOfRays = _numberOfHits = 0;
  scan(image);
  printf("\nNumber of rays: %llu", _numberOfRays);
  printf("\nNumber of hits: %llu", _numberOfHits);
  printElapsedTime("\nDONE! ", clock() - t);
}

void
RayTracer::setPixelRay(float x, float y)
//[]---------------------------------------------------[]
//|  Set pixel ray                                      |
//|  @param x coordinate of the pixel                   |
//|  @param y cordinates of the pixel                   |
//[]---------------------------------------------------[]
{
  auto p = imageToWindow(x, y);

  switch (_camera->projectionType())
  {
    case Camera::Perspective:
      _pixelRay.direction = (p - _camera->nearPlane() * _vrc.n).versor();
      break;

    case Camera::Parallel:
      _pixelRay.origin = _camera->transform()->position() + p;
      break;
  }
}

void
RayTracer::scan(Image& image)
{
  ImageBuffer scanLine{_W, 1};

  for (int j = 0; j < _H; j++)
  {
    auto y = (float)j + 0.5f;

    printf("Scanning line %d of %d\r", j + 1, _H);
    for (int i = 0; i < _W; i++)
      scanLine[i] = shoot((float)i + 0.5f, y);
    image.setData(0, j, scanLine);
  }
}

Color
RayTracer::shoot(float x, float y)
//[]---------------------------------------------------[]
//|  Shoot a pixel ray                                  |
//|  @param x coordinate of the pixel                   |
//|  @param y cordinates of the pixel                   |
//|  @return RGB color of the pixel                     |
//[]---------------------------------------------------[]
{
  // set pixel ray
  setPixelRay(x, y);

  // trace pixel ray
  Color color = trace(_pixelRay, 0, 1.0f);

  // adjust RGB color
  if (color.r > 1.0f)
    color.r = 1.0f;
  if (color.g > 1.0f)
    color.g = 1.0f;
  if (color.b > 1.0f)
    color.b = 1.0f;
  // return pixel color
  return color;
}

Color
RayTracer::trace(const Ray& ray, uint32_t level, float weight)
//[]---------------------------------------------------[]
//|  Trace a ray                                        |
//|  @param the ray                                     |
//|  @param recursion level                             |
//|  @param ray weight                                  |
//|  @return color of the ray                           |
//[]---------------------------------------------------[]
{
  if (level > _maxRecursionLevel)
    return Color::black;
  _numberOfRays++;

  Intersection hit;

  return intersect(ray, hit) ? shade(ray, hit, level, weight) : background();
}

inline constexpr auto
rt_eps()
{
  return 1e-4f;
}

bool
RayTracer::intersect(const Ray& ray, Intersection& hit)
//[]---------------------------------------------------[]
//|  Ray/object intersection                            |
//|  @param the ray (input)                             |
//|  @param information on intersection (output)        |
//|  @return true if the ray intersects an object       |
//[]---------------------------------------------------[]
{
  hit.object = nullptr;
  hit.distance = ray.tMax;
  // TODO: insert your code here

  
  vec3f b;
  int index;
  float distance = ray.tMax;
  auto scene = this->scene();

  // Percorro a minha cena para verificar se o raio intercepta a minha box
  for (auto it : scene->getIteratorPrimitives()) {
    // Caso o meu objeto seja um primitivo
    if (auto p = dynamic_cast<Primitive*>(it.get())) {
      // Verifico se meu objeto é visivel na cena
      if (p->sceneObject()->visible) {
        
        auto t = p->transform();
        auto origem = t->worldToLocalMatrix().transform(ray.origin);
        auto direction = t->worldToLocalMatrix().transformVector(ray.direction);
        auto d = math::inverse(direction.length());

        Ray r{ origem, direction };

        // Caso o meu raio intercepta o meu primitivo
        if (p->getBVH()->intersect(r, hit, d)) {
          _numberOfHits++;
          if (hit.distance < distance) {
            hit.object = p;
            distance = hit.distance;
          }
        }
      }
    }
  }

  return hit.object != nullptr;
}

Color
RayTracer::shade(const Ray& ray, Intersection& hit, int level, float weight)
//[]---------------------------------------------------[]
//|  Shade a point P                                    |
//|  @param the ray (input)                             |
//|  @param information on intersection (input)         |
//|  @param recursion level                             |
//|  @param ray weight                                  |
//|  @return color at point P                           |
//[]---------------------------------------------------[]
{
  // TODO: insert your code here

  auto scene = this->scene();
  auto objMaterial = hit.object->material;
  // OA * IA
  Color c = hit.object->material.ambient * scene->ambientLight;

  auto mesh = hit.object->mesh()->data();
  auto triangle = mesh.triangles[hit.triangleIndex];

  mat3f normalMatrix = mat3f{ hit.object->sceneObject()->transform()->worldToLocalMatrix() }.transposed();

  auto n0 = mesh.vertexNormals[triangle.v[0]];
  auto n1 = mesh.vertexNormals[triangle.v[1]];
  auto n2 = mesh.vertexNormals[triangle.v[2]];
  
  // calcula o valor de N local
  vec3f N = hit.p.x * n0 + hit.p.y * n1 + hit.p.z * n2;
  N = (normalMatrix * N).normalize();
  
  vec3f P = (ray.origin + hit.distance * ray.direction) + rt_eps() * N;

  if (N.dot(ray.direction) > 0.0f) {
    N = -N;
  }

  vec3f V = (camera()->transform()->position() - P).normalize();

  Color il = Color::black;

  // Percorro por todas as minhas luzes da minha cena
  for (int i = 0; i < NL; ++i) {
    if (scene->light[i] != nullptr) {
      auto light = scene->light[i];

      vec3f LightPosition = light->transform()->position();
      vec3f LightDirection = -(light->transform()->up().versor());

      auto L = light->type() == Light::Type::Directional ? LightDirection : (P - LightPosition);
      auto d = L.length();
      L = L.versor();

      Ray r = light->type() == Light::Type::Directional ? Ray{ P, -L } : Ray{ P, -L, 0.0f, d };

      if (!shadow(r)) {

        if (light->type() == Light::Type::Directional) {
          il = light->color;
        }
        else if (light->type() == Light::Type::Point) {
          il = light->color * math::inverse(pow(d, light->falloff()));
        }
        else if (light->type() == Light::Type::Spot) {
          auto Ll = L * math::inverse(d);

          auto theta = math::toRadians(light->angle());
          auto psi = Ll.dot(LightDirection);

          Color color = light->color * math::inverse(pow(d, light->falloff())) * pow(psi, light->spotExponent());

          il = theta > acos(psi) ? color : Color::black;
        }

        auto R = (reflect(L, N)).versor();

        Color Difuse = (objMaterial.diffuse * il) * std::max(N.dot(-L), 0.0f);
        Color Speculate = (objMaterial.spot * il) * pow(std::max(R.dot(V), 0.0f), objMaterial.shine);

        c += Difuse + Speculate;

      }

    }
  }

    auto Or = objMaterial.specular;
    
    if (Or != Color::black) {

      float w = weight * std::max({Or.r, Or.g, Or.b});
      if (w > _minWeight) {

        auto RL = reflect(ray.direction, N);
        Ray reflectRay{ P, RL };

        auto seconds = trace(reflectRay, level + 1, w);

        if (seconds != _scene->backgroundColor) {
          c += Or * seconds;
        }

       
      }

    }

  return c;
}

inline vec3f
RayTracer::reflect(vec3f L, vec3f N){
  return L - 2 * (N.dot(L)) * N;
}

Color
RayTracer::background() const
//[]---------------------------------------------------[]
//|  Background                                         |
//|  @return background color                           |
//[]---------------------------------------------------[]
{
  return _scene->backgroundColor;
}

bool
RayTracer::shadow(const Ray& ray)
//[]---------------------------------------------------[]
//|  Verifiy if ray is a shadow ray                     |
//|  @param the ray (input)                             |
//|  @return true if the ray intersects an object       |
//[]---------------------------------------------------[]
{
  Intersection hit;
  return intersect(ray, hit);
}

} // end namespace cg
