//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: Scene.h
// ========
// Class definition for scene.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 21/09/2019

#ifndef __Scene_h
#define __Scene_h

// Numero de luzes na minha scene
#define NL 100

#include "SceneObject.h"
#include "graphics/Color.h"
#include "Primitive.h"
#include "light.h"
#include <list>

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// Scene: scene class
// =====
class Scene: public SceneNode
{
public:
  Color backgroundColor{Color::gray};
  Color ambientLight{Color::black};

  Reference<Light> light[NL] = { nullptr };
  int lightsOn;
  int sizeLight;

  /// Constructs an empty scene.
  Scene(const char* name):
    SceneNode{name},
    lightsOn{ 0 },
    sizeLight{ 0 }
  {
    RootObject = new SceneObject("root", *this);
  }

  ~Scene() {
    clearList();
  }

  void release() {
    clearList();

    for (int i = 0; i < NL; ++i) {
      light[i] = nullptr;
    }
  }

  // Fun��o para atualizar o numero de luzes na minha scene
  void updateLightsOn() {

    // Itera sobre o meu numero maximo de elementos, depois atualiza para uma posi��o vazia do meu vetor
    for (int i = 0; i < NL; ++i) {

      // Caso eu encontre uma posi��o vazia em meu vetor
      if (light[i] == nullptr) {
        lightsOn = i;
        break;
      }
    }

  }

  bool addLight(Reference<Light> newLight) {

    bool ret = false;

    if (sizeLight < NL) {
      light[lightsOn] = newLight;
      // Altero a flag que indica que a luz esta ativa
      light[lightsOn]->switchLight();
      updateLightsOn();

      sizeLight += 1;

      ret = true;
    }

    return ret;
  }

  void removeLight(Reference<Light> rlight) {

    for (int i = 0; i < NL; ++i) {
      if (light[i] == rlight) {
        light[i]->switchLight();
        light[i] = nullptr;
        updateLightsOn();

        sizeLight -= 1;
      }
    }


  }

  auto getRoot() {
    return RootObject;
  }

  // Retorna se minha lista de primitivos esta vazia
  bool isEmpty() {
    return components.empty();
  }

  auto size() {
    return components.size();
  }

  // Limpa a minha lista de primitivos
  void clearList() {
    components.clear();
  }

  // Adiciono o primitivo a minha lista de primitivos
  void add(Reference<Component> component) {
    components.push_back(component);
  }
  // Removo o meu primitivo da minha lista de primitivos
  void remove(Reference<Component> component) {
    components.remove(component);
  }

  auto getIteratorPrimitives() {
    return components;
  }

  bool containsComponent(Reference<Component> component) {
    bool contains = false;

    if (!components.empty()) {
      for (auto it = components.begin(); it != components.end(); ++it) {
        if (*it == component) {
          contains = true;
          break;
        }
      }
    }

    return contains;
  }

private:
  Reference<SceneObject> RootObject;

  // Lista de objetos que seram renderizados
  std::list<Reference<Component>> components;

}; // Scene

} // end namespace cg

#endif // __Scene_h
