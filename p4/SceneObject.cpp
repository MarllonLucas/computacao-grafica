//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: SceneObject.cpp
// ========
// Source file for scene object.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 07/09/2019

#include "SceneObject.h"
#include "Scene.h"
#include "Camera.h"

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// SceneObject implementation
// ===========
  SceneObject::~SceneObject() {
    components.clear();
    objects_scene.clear();
  }

  bool
    SceneObject::isFather(Reference<SceneObject> object, Reference<SceneObject> reference) {
    if (object->parent() == scene()->getRoot()) {
      return true;
    }
    else {

      if (object->parent() == reference) {
        return false;
      }
      else {
        return isFather(object->parent(), reference);
      }
    }
  }

  void
    SceneObject::setParent(SceneObject* parent)
  {

    // pega o pai 
    auto father = this->parent();

    // Pega o no da minha raiz que est� sevindo como uma header para os meus objetos
    auto root = this->scene()->getRoot();

    /*
      passo 0: verificar se o meu pai � o meu root
        - Se for root: remover do root
        - Se nao: remover do pai
      passo 1: atualiza para o pai atual (_parent to parent)
      passo 2: adicionar na nova cole��o
    */

    if (parent == nullptr) {

      // Verifica se existe um ponteiro no pai
      // caso exita eu removo esse objeto dessa lista em questao
      if (father != nullptr) {
        father->remove(this);
      }

      root->add(this);
      this->_parent = root;

    }
    else {

      if (father != nullptr) {
        father->remove(this);
      }

      parent->add(this);
      this->_parent = parent;

    }

    transform()->parentChanged();
  }

  void
    SceneObject::setParentEditor(SceneObject* parent)
  {
    this->_parent = parent;
  }

  // add object in the list
  void
    SceneObject::add(Reference<SceneObject> object) {

    if (!objects_scene.empty()) {
      for (auto it = object->getComponentIteratorBegin(); it != object->getComponentIteratorEnd(); ++it) {

        bool contains = _scene->containsComponent(*it);

        if (!contains) {
          if (auto primitive = dynamic_cast<Primitive*>((*it).get()))
            _scene->add(primitive);
          else if (auto camera = dynamic_cast<Camera*>((*it).get()))
            _scene->add(camera);

        }

      }

    }
    objects_scene.push_back(object);
  }

  void
    SceneObject::remove(Reference<SceneObject> object) {
    // Remove o objeto requisitado da minha lista de objetos

    for (auto it : object->getComponentIterator()) {
      auto primitive = dynamic_cast<Primitive*>(it.get());
      auto camera = dynamic_cast<Camera*>(it.get());
      auto light = dynamic_cast<Light*>(it.get());

      if (primitive)
        _scene->remove(primitive);

      if (camera)
        _scene->remove(camera);

      if (light) {
        _scene->removeLight(light);
      }
    }

    objects_scene.remove(object);
  }

  void
    SceneObject::add(Reference<Component> component) {

    // Nao deixa adicionar mais de um componente de um mesmo tipo
    if (containsComponent(component))
      return;

    // Adicionar o componente na lista de componentes
    components.push_back(component);

    if (auto primitive = dynamic_cast<Primitive*>(component.get()))
      _scene->add(primitive);
    if (auto camera = dynamic_cast<Camera*>(component.get()))
      _scene->add(camera);

    component->_sceneObject = this;

  }

  bool SceneObject::containsComponent(Reference<Component> component) {

    bool contains = false;

    if (!components.empty()) {

      for (auto it = components.begin(); it != components.end(); ++it) {

        if ((*it) == component) {
          contains = true;
          break;
        }

      }

    }

    return contains;

  }

  Component*
    SceneObject::primitive() {

    for (auto it : components) {
      if (auto p = dynamic_cast<Primitive*>(it.get())) {
        return p;
      }
    }

    return nullptr;
  }
  Component*
    SceneObject::light() {
    for (auto it : components) {
      if (auto p = dynamic_cast<Light*>(it.get())) {
        return p;
      }
    }

    return nullptr;
  }

  Component*
    SceneObject::camera() {
    for (auto it : components) {
      if (auto p = dynamic_cast<Camera*>(it.get())) {
        return p;
      }
    }

    return nullptr;
  }

  void
    SceneObject::remove(Reference<Component> component) {

    /*Remove os compenente da minha cena*/
    if (auto camera = dynamic_cast<Camera*>(component.get()))
      _scene->remove(camera);

    if (auto primitive = dynamic_cast<Primitive*>(component.get())) {
      _scene->remove(primitive);
    }

    if (auto light = dynamic_cast<Light*>(component.get())) {
      _scene->removeLight(light);
    }

    components.remove(component);
  }

} // end namespace cg
