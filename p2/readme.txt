Nome: Marllon Lucas Rodrigues Rosa
Nome: Pedro Flores

Todos as atividades foram implementadas neste trabalho

Teclas de atalhos implementadas:

- Deletar um objeto corrente (caso seja diferente do objeto 'SCENE') -- Delete
- Adicionar um objeto do tipo BOX como filho do objeto corrente selecionado -- CTRL + N
- Adicionar um objeto do tipo SPHERE como filho do objeto corrente selecionado -- CTRL + M
- Adicionar um objeto do tipo CAMERA como filho do objeto corrente selecionado -- CTRL + K

Trocar o parentesco dos objetos, foi implementado uma funcionalidade do tipo DRAGDROP, no qual pode-se arrastar um objeto para outro e o mesmo
irá alterar o parentesco.

Para dar foco em um objeto basta pressionar as teclas "ALT + F"
Para realizar o foco, "jogamos" a carmera para a mesma posição do objeto corrente selecionado, após este passo fez-se um translação de
{0, 0, distancia} para que a camera não ficasse dentro do objeto

a distancia é dado por pitagoras, onde pego o valor da hipotenusa de um triangulo formado pela metade da altura do objeto e o angulo de vista da camera

a formula é dada por:

distanciaY = sacala.y / tangente(angulo_de_vista / 2)
distanciaX = sacala.y / tangente(angulo_de_vista / 2)

distancia = (distanciaY + distanciaX + scala.z)

pela qual essa formula é passada para o redirecinamento do foco no eixo Z
