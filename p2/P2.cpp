#include "geometry/MeshSweeper.h"
#include "P2.h"
#include <list>

MeshMap P2::_defaultMeshes;

inline void
P2::buildDefaultMeshes()
{
  _defaultMeshes["None"] = nullptr;
  _defaultMeshes["Box"] = GLGraphics3::box();
  _defaultMeshes["Sphere"] = GLGraphics3::sphere();
}

inline Primitive*
makePrimitive(MeshMapIterator mit)
{
  return new Primitive{mit->second, mit->first};
}

inline void
P2::buildScene()
{
  _current = _scene = new Scene{"Scene 1"};
  _editor = new SceneEditor{*_scene};
  _editor->setDefaultView((float)width() / (float)height());

  auto camera = createCamera(_scene->getRoot(), "Main Camera");
  auto box    = createNode(_scene->getRoot(), 1, "Box");

  Camera::setCurrent(camera);
}

Reference<Camera> 
P2::createCamera(Reference<SceneObject> parent, std::string name) {
  auto parentScene = parent->scene();
  auto object = new SceneObject{ name.c_str(), * parentScene };

  Reference<Camera> camera = new Camera;

  object->add(Reference<Component>(camera));
  object->setParent(parent);

  return camera;
}

Reference<SceneObject>
P2::createNode(Reference<SceneObject> parent, int typeNode, std::string shape) {

  /*
    TypeNode:
      0: Box
      1: Empty Object
      2: Camera
  */

  std::string name = typeNode == 0 ? "Empyt Object " : (typeNode == 1 ? "Box " : "Sphere ");
  // Depois verificar se o objeto que eu quero instaciar como filho seja igual a minha cena passada
  auto parentScene = parent->scene();

  // Pega o nome do meu objeto e adiciona um valor inteiro para o nome 
  name = name + std::to_string(getIndexObjects());

  auto newObject = new SceneObject{ name.c_str(), *parentScene };

  // Verifico se o parent passado � igual a raiz da minha sena
  if (parentScene->getRoot() == parent) newObject->setParent(nullptr);
  else                                  newObject->setParent(parent);

  // Caso o mesmo seja uma box criada
  // Devo adicionar um componente primitivo nele
  if (typeNode) {
    auto primitive = makePrimitive(_defaultMeshes.find(shape));
    newObject->add(Reference<Component>(primitive));
  }

  return newObject;

}

void
P2::initialize()
{
  Application::loadShaders(_program, "shaders/p2.vs", "shaders/p2.fs");
  Assets::initialize();
  buildDefaultMeshes();
  buildScene();
  _renderer = new GLRenderer{*_scene};
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(1.0f, 1.0f);
  glEnable(GL_LINE_SMOOTH);
  _program.use();
}

namespace ImGui
{
  void ShowDemoWindow(bool*);
}

void
P2::hierarchyWindowRecursive(Reference<SceneObject> node) {
  bool openNode = false;
  ImGuiTreeNodeFlags flag{ ImGuiTreeNodeFlags_OpenOnArrow };

  if (node != node->scene()->getRoot()) {

    // Verifica se eu estou em um n� intermediario
    if (!node->getObjectEmpty()) {
      openNode = ImGui::TreeNodeEx(node, _current == node ? flag | ImGuiTreeNodeFlags_Selected : flag, node->name());
    }
    else {
      flag |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
      ImGui::TreeNodeEx(node, _current == node ? flag | ImGuiTreeNodeFlags_Selected : flag, node->name());
    }

    if (ImGui::IsItemClicked()) _current = node;

    if (ImGui::BeginDragDropSource()) {

      ImGui::Text(node->name());
      ImGui::SetDragDropPayload("SceneObject", &node, sizeof(node));
      ImGui::EndDragDropSource();
    
    }

    if (ImGui::BeginDragDropTarget()) {

      if (auto* payload = ImGui::AcceptDragDropPayload("SceneObject")) {

        // Pega o objeto enviado
        auto obj = *(cg::Reference<SceneObject>*)payload->Data;

        bool isFather = obj->isFather(node, obj);

        if (isFather) {
          // Faz o ajuste de parametros
          obj->setParent(node);
        }
        else {
          std::cout << "Nao foi possivel fazer a alteracao" << std::endl;
        }
        

      }

      ImGui::EndDragDropTarget();
    }

    

  }

  // Verifico se o meu no esta aberto na minha interface, ou se o mesmo � a minha raiz
  if (openNode || _scene->getRoot() == node) {

    for (auto it : node->getObjectsIterator())
      hierarchyWindowRecursive(it);

    ImGui::TreePop();

  }

}

inline void
P2::removeCurrent() {
    ImGui::OpenPopup("Delete?");
  if (ImGui::BeginPopupModal("Delete?", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {

    auto box = dynamic_cast<SceneObject*>(_current);

    std::string texto;

    if (box)
      texto = "Tem certeza que deja apagar este objeto e todos os seu filhos?";
    else
      texto = "Ainda nao eh possivel fazer a remocao de uma cena!";

    ImGui::Text(texto.c_str());
    ImGui::Separator();


    if (box) {
      if (ImGui::Button("OK", ImVec2(120, 0))) {
        // Chamo a fun��o para a remo��o do objeto
        removeObject();
        // Removo a minha popup
        ImGui::CloseCurrentPopup();
      }
      ImGui::SetItemDefaultFocus();
      ImGui::SameLine();
    }


    if (ImGui::Button("Cancelar", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
    ImGui::EndPopup();
  }
}

inline void
P2::removeObject() {
  if (auto object = dynamic_cast<SceneObject*>(_current)) {

    auto father = object->parent();
    father->remove(object);

    if (father == father->scene()->getRoot())
      _current = father->scene();
    else
      _current = father;
  }
}


inline void
P2::hierarchyWindow()
{
  ImGui::Begin("Hierarchy");
  if (ImGui::Button("Create###object"))
    ImGui::OpenPopup("CreateObjectPopup");
  if (ImGui::BeginPopup("CreateObjectPopup"))
  {
    // pega um ponteiro do tipo SceneObject
    auto emptyObj = dynamic_cast<SceneObject*>(_current);
    if (ImGui::MenuItem("Empty Object"))
    {
      // Cria um n� do tipo box
      auto newEmpty = emptyObj ? createNode(emptyObj, 0, "None") : createNode(_scene->getRoot(), 0, "None");
    }
    if (ImGui::BeginMenu("3D Object"))
    {
      if (ImGui::MenuItem("Box"))
      {
        // Cria um n� do tipo box
        auto newEmpty = emptyObj ? createNode(emptyObj, 1, "Box") : createNode(_scene->getRoot(), 1, "Box");
      }
      if (ImGui::MenuItem("Sphere"))
      {
        // Cria um n� do tipo box
        auto newEmpty = emptyObj ? createNode(emptyObj, 2, "Sphere") : createNode(_scene->getRoot(), 2, "Sphere");
      }
      ImGui::EndMenu();
    }
    if (ImGui::MenuItem("Camera"))
    {
      std::string name = "Camera " + std::to_string(getIndexObjects());
      auto camera = emptyObj ? createCamera(emptyObj, name.c_str()) : createCamera(_scene->getRoot(), name.c_str());
    }
    ImGui::EndPopup();
  }

  ImGui::SameLine();
  if (ImGui::Button("Delete"))
    removeObject();
    

  ImGui::Separator();

  ImGuiTreeNodeFlags flag{ ImGuiTreeNodeFlags_OpenOnArrow };
  auto open = ImGui::TreeNodeEx(_scene, _current == _scene ? flag | ImGuiTreeNodeFlags_Selected : flag, _scene->name());

  if (ImGui::IsItemClicked())
    _current = _scene;

  if (ImGui::BeginDragDropTarget()) {
    if (auto* payload = ImGui::AcceptDragDropPayload("SceneObject")) {
      // Pega o objeto enviado
      auto obj = *(cg::Reference<SceneObject>*)payload->Data;
      obj->setParent(_scene->getRoot());
    }

    ImGui::EndDragDropTarget();
  }

  if (open) {
    // TODO: fazer uma verifica��o para uma cena
    hierarchyWindowRecursive(_scene->getRoot());
  }

  ImGui::End();
}

namespace ImGui
{ // begin namespace ImGui

void
ObjectNameInput(NameableObject* object)
{
  const int bufferSize{128};
  static NameableObject* current;
  static char buffer[bufferSize];

  if (object != current)
  {
    strcpy_s(buffer, bufferSize, object->name());
    current = object;
  }
  if (ImGui::InputText("Name", buffer, bufferSize))
    object->setName(buffer);
}

inline bool
ColorEdit3(const char* label, Color& color)
{
  return ImGui::ColorEdit3(label, (float*)&color);
}

inline bool
DragVec3(const char* label, vec3f& v)
{
  return DragFloat3(label, (float*)&v, 0.1f, 0.0f, 0.0f, "%.2g");
}

void
TransformEdit(Transform* transform)
{
  vec3f temp;

  temp = transform->localPosition();
  if (ImGui::DragVec3("Position", temp))
    transform->setLocalPosition(temp);
  temp = transform->localEulerAngles();
  if (ImGui::DragVec3("Rotation", temp))
    transform->setLocalEulerAngles(temp);
  temp = transform->localScale();
  if (ImGui::DragVec3("Scale", temp)) {
    
    if (temp.x < 0.001)
      temp.x = 0.001;
    if (temp.y < 0.001)
      temp.y = 0.001;
    if (temp.y < 0.001)
      temp.y = 0.001;

    transform->setLocalScale(temp);
  }
    
}

} // end namespace ImGui

inline void
P2::sceneGui()
{
  auto scene = (Scene*)_current;

  ImGui::ObjectNameInput(_current);
  ImGui::Separator();
  if (ImGui::CollapsingHeader("Colors"))
  {
    ImGui::ColorEdit3("Background", scene->backgroundColor);
    ImGui::ColorEdit3("Ambient Light", scene->ambientLight);
  }
}

inline void
P2::inspectPrimitive(Primitive& primitive)
{
  char buffer[16];

  snprintf(buffer, 16, "%s", primitive.meshName());
  ImGui::InputText("Mesh", buffer, 16, ImGuiInputTextFlags_ReadOnly);
  if (ImGui::BeginDragDropTarget())
  {
    if (auto* payload = ImGui::AcceptDragDropPayload("PrimitiveMesh"))
    {
      auto mit = *(MeshMapIterator*)payload->Data;
      primitive.setMesh(mit->second, mit->first);
    }
    ImGui::EndDragDropTarget();
  }
  ImGui::SameLine();
  if (ImGui::Button("...###PrimitiveMesh"))
    ImGui::OpenPopup("PrimitiveMeshPopup");
  if (ImGui::BeginPopup("PrimitiveMeshPopup"))
  {
    auto& meshes = Assets::meshes();

    if (!meshes.empty())
    {
      for (auto mit = meshes.begin(); mit != meshes.end(); ++mit)
        if (ImGui::Selectable(mit->first.c_str()))
          primitive.setMesh(Assets::loadMesh(mit), mit->first);
      ImGui::Separator();
    }
    for (auto mit = _defaultMeshes.begin(); mit != _defaultMeshes.end(); ++mit)
      if (ImGui::Selectable(mit->first.c_str()))
        primitive.setMesh(mit->second, mit->first);
    ImGui::EndPopup();
  }
  ImGui::ColorEdit3("Mesh Color", (float*)&primitive.color);
}

void
P2::inspectCamera(Camera& camera)
{
  static const char* projectionNames[]{"Perspective", "Orthographic"};
  auto cp = camera.projectionType();

  if (ImGui::BeginCombo("Projection", projectionNames[cp]))
  {
    for (auto i = 0; i < IM_ARRAYSIZE(projectionNames); ++i)
    {
      auto selected = cp == i;

      if (ImGui::Selectable(projectionNames[i], selected))
        cp = (Camera::ProjectionType)i;
      if (selected)
        ImGui::SetItemDefaultFocus();
    }
    ImGui::EndCombo();
  }
  camera.setProjectionType(cp);
  if (cp == View3::Perspective)
  {
    auto fov = camera.viewAngle();

    if (ImGui::SliderFloat("View Angle",
      &fov,
      MIN_ANGLE,
      MAX_ANGLE,
      "%.0f deg",
      1.0f))
      camera.setViewAngle(fov <= MIN_ANGLE ? MIN_ANGLE : fov);
  }
  else
  {
    auto h = camera.height();

    if (ImGui::DragFloat("Height",
      &h,
      MIN_HEIGHT * 10.0f,
      MIN_HEIGHT,
      math::Limits<float>::inf()))
      camera.setHeight(h <= 0 ? MIN_HEIGHT : h);
  }

  float n;
  float f;

  camera.clippingPlanes(n, f);

  if (ImGui::DragFloatRange2("Clipping Planes",
    &n,
    &f,
    MIN_DEPTH,
    MIN_DEPTH,
    math::Limits<float>::inf(),
    "Near: %.2f",
    "Far: %.2f"))
  {
    if (n <= 0)
      n = MIN_DEPTH;
    if (f - n < MIN_DEPTH)
      f = n + MIN_DEPTH;
    camera.setClippingPlanes(n, f);
  }
}

inline void
P2::addComponentButton(SceneObject& object)
{
  if (ImGui::Button("Add Component"))
    ImGui::OpenPopup("AddComponentPopup");
  if (ImGui::BeginPopup("AddComponentPopup"))
  {
    // Flag da camera
    bool c = true;
    // Flag do primitivo
    bool p = true;
    // Devo verificar se o objeto ja cont�m um primitivo
    for (auto it : object.getComponentIterator()) {
      if (auto primitive = dynamic_cast<Primitive*>(it.get())) {
        p = false;
      }
      if (auto camera = dynamic_cast<Camera*>(it.get())) {
        c = false;
      }
    }

    if (ImGui::MenuItem("Primitive"))
    {
      // caso eu possa adicionar um primitivo no meu objeto
      if (p) {
        auto primitive = makePrimitive(_defaultMeshes.find("Box"));
        object.add(Reference<Component>(primitive));
      }
      else {

        std::cout << "Nao foi possivel adicionar um primitivo" << std::endl;

      }
    }
    if (ImGui::MenuItem("Camera"))
    {
      if (c) {

        Reference<Camera> camera = new Camera;
        object.add(Reference<Component>(camera));

      }
      else {

        std::cout << "Nao foi possivel adicionar uma camera" << std::endl;

      }
    }
    ImGui::EndPopup();
  }
}

inline void
P2::sceneObjectGui()
{
  auto object = (SceneObject*)_current;

  addComponentButton(*object);
  ImGui::Separator();
  ImGui::ObjectNameInput(object);
  ImGui::SameLine();
  ImGui::Checkbox("visible", &object->visible);
  ImGui::Separator();
  if (ImGui::CollapsingHeader(object->transform()->typeName()))
    ImGui::TransformEdit(object->transform());

  for (auto it : object->getComponentIterator()) {

    // Caso o componente atual seja do tipo primitivo
    if (auto component = dynamic_cast<Primitive*>(it.get())) {

      auto notDelete{ true };
      auto open = ImGui::CollapsingHeader(component->typeName(), &notDelete);

      if (!notDelete) {
        object->remove(Reference<Component>(component));
      }
      else if (open)
        inspectPrimitive(*component);
    }

    else if (auto component = dynamic_cast<Camera*>(it.get())) {

      auto notDelete{ true };
      auto open = ImGui::CollapsingHeader(component->typeName(), &notDelete);

      if (!notDelete) {
        object->remove(Reference<Component>(component));
      }
      else if (open)
      {
        auto isCurrent = component == Camera::current();

        ImGui::Checkbox("Current", &isCurrent);
        Camera::setCurrent(isCurrent ? component : nullptr);
        inspectCamera(*component);
      }

    }
  }
}

inline void
P2::objectGui()
{
  if (_current == nullptr)
    return;
  if (dynamic_cast<SceneObject*>(_current))
  {
    sceneObjectGui();
    return;
  }
  if (dynamic_cast<Scene*>(_current))
    sceneGui();
}

inline void
P2::inspectorWindow()
{
  ImGui::Begin("Inspector");
  objectGui();
  ImGui::End();
}

inline void
P2::editorViewGui()
{
  if (ImGui::Button("Set Default View"))
    _editor->setDefaultView(float(width()) / float(height()));
  ImGui::Separator();

  auto t = _editor->camera()->transform();
  vec3f temp;

  temp = t->localPosition();
  if (ImGui::DragVec3("Position", temp))
    t->setLocalPosition(temp);
  temp = t->localEulerAngles();
  if (ImGui::DragVec3("Rotation", temp))
    t->setLocalEulerAngles(temp);
  inspectCamera(*_editor->camera());
  ImGui::Separator();
  ImGui::Checkbox("Show Ground", &_editor->showGround);
}

inline void
P2::assetsWindow()
{
  if (!_showAssets)
    return;
  ImGui::Begin("Assets");
  if (ImGui::CollapsingHeader("Meshes"))
  {
    auto& meshes = Assets::meshes();

    for (auto mit = meshes.begin(); mit != meshes.end(); ++mit)
    {
      auto meshName = mit->first.c_str();
      auto selected = false;

      ImGui::Selectable(meshName, &selected);
      if (ImGui::BeginDragDropSource())
      {
        Assets::loadMesh(mit);
        ImGui::Text(meshName);
        ImGui::SetDragDropPayload("PrimitiveMesh", &mit, sizeof(mit));
        ImGui::EndDragDropSource();
      }
    }
  }
  ImGui::Separator();
  if (ImGui::CollapsingHeader("Textures"))
  {
    // p3
  }
  ImGui::End();
}

inline void
P2::editorView()
{
  if (!_showEditorView)
    return;
  ImGui::Begin("Editor View Settings");
  editorViewGui();
  ImGui::End();
}

inline void
P2::fileMenu()
{
  if (ImGui::MenuItem("New"))
  {
    // TODO
  }
  if (ImGui::MenuItem("Open...", "Ctrl+O"))
  {
    // TODO
  }
  ImGui::Separator();
  if (ImGui::MenuItem("Save", "Ctrl+S"))
  {
    // TODO
  }
  if (ImGui::MenuItem("Save As..."))
  {
    // TODO
  }
  ImGui::Separator();
  if (ImGui::MenuItem("Exit", "Alt+F4"))
  {
    shutdown();
  }
}

inline bool
showStyleSelector(const char* label)
{
  static int style = 1;

  if (!ImGui::Combo(label, &style, "Classic\0Dark\0Light\0"))
    return false;
  switch (style)
  {
    case 0: ImGui::StyleColorsClassic();
      break;
    case 1: ImGui::StyleColorsDark();
      break;
    case 2: ImGui::StyleColorsLight();
      break;
  }
  return true;
}

inline void
P2::showOptions()
{
  ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.6f);
  showStyleSelector("Color Theme##Selector");
  ImGui::ColorEdit3("Selected Wireframe", _selectedWireframeColor);
  ImGui::PopItemWidth();
}

inline void
P2::mainMenu()
{
  if (ImGui::BeginMainMenuBar())
  {
    if (ImGui::BeginMenu("File"))
    {
      fileMenu();
      ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("View"))
    {
      if (Camera::current() == 0)
        ImGui::MenuItem("Edit View", nullptr, true, false);
      else
      {
        static const char* viewLabels[]{"Editor", "Renderer"};

        if (ImGui::BeginCombo("View", viewLabels[_viewMode]))
        {
          for (auto i = 0; i < IM_ARRAYSIZE(viewLabels); ++i)
          {
            if (ImGui::Selectable(viewLabels[i], _viewMode == i))
              _viewMode = (ViewMode)i;
          }
          ImGui::EndCombo();
        }
      }
      ImGui::Separator();
      ImGui::MenuItem("Assets Window", nullptr, &_showAssets);
      ImGui::MenuItem("Editor View Settings", nullptr, &_showEditorView);
      ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Tools"))
    {
      if (ImGui::BeginMenu("Options"))
      {
        showOptions();
        ImGui::EndMenu();
      }
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
}

void
P2::gui()
{
  mainMenu();
  hierarchyWindow();
  inspectorWindow();
  assetsWindow();
  editorView();

  /*
  static bool demo = true;
  ImGui::ShowDemoWindow(&demo);
  */
}

inline void
drawMesh(GLMesh* mesh, GLuint mode)
{
  glPolygonMode(GL_FRONT_AND_BACK, mode);
  glDrawElements(GL_TRIANGLES, mesh->vertexCount(), GL_UNSIGNED_INT, 0);
}

inline void
P2::drawPrimitive(Primitive& primitive)
{
  auto m = glMesh(primitive.mesh());

  if (nullptr == m)
    return;

  auto t = primitive.transform();
  auto normalMatrix = mat3f{t->worldToLocalMatrix()}.transposed();

  _program.setUniformMat4("transform", t->localToWorldMatrix());
  _program.setUniformMat3("normalMatrix", normalMatrix);
  _program.setUniformVec4("color", primitive.color);
  _program.setUniform("flatMode", (int)0);
  m->bind();
  drawMesh(m, GL_FILL);
  if (primitive.sceneObject() != _current)
    return;
  _program.setUniformVec4("color", _selectedWireframeColor);
  _program.setUniform("flatMode", (int)1);
  drawMesh(m, GL_LINE);
}

inline float
convertToRad(float theta) {
  return theta*(M_PI/180);
}

inline void
P2::drawCamera(Camera& camera)
{
  float F, B, viewAngle, H_min, H_max;
  auto BF = camera.clippingPlanes(F, B);

  B = B * 0.7;

  auto M = mat4f{ camera.cameraToWorldMatrix() };

  if (camera.projectionType() == Camera::ProjectionType::Perspective) {
    viewAngle = convertToRad(camera.viewAngle());

    H_min = 2 * F * tanf(viewAngle / 2);
    H_max = 2 * B * tanf(viewAngle / 2);
  }
  else {

    H_min = H_max = camera.height();

  }

  

  vec3f P1, P2, P3, P4, P5, P6, P7, P8;

  /* Linhas
    Square min
    P1 - P2
    P2 - P3
    P3 - P4
    P4 - P1
    Square max
    P5 - P6
    P6 - P7
    P7 - P8
    P8 - P5

    P1 - P5
    P2 - P6
    P3 - P7
    P4 - P8

  */

  P1 = M.transform(vec3f{ H_min / 2 ,  H_min / 2, -F });
  P2 = M.transform(vec3f{-H_min / 2 ,  H_min / 2, -F });
  P3 = M.transform(vec3f{-H_min / 2 , -H_min / 2, -F });
  P4 = M.transform(vec3f{ H_min / 2 , -H_min / 2, -F });

  P5 = M.transform(vec3f{ H_max / 2 ,  H_max / 2, -B });
  P6 = M.transform(vec3f{-H_max / 2 ,  H_max / 2, -B });
  P7 = M.transform(vec3f{-H_max / 2 , -H_max / 2, -B });
  P8 = M.transform(vec3f{ H_max / 2 , -H_max / 2, -B });

  _editor->setLineColor(Color::black);

  _editor->drawLine(P1, P2);
  _editor->drawLine(P2, P3);
  _editor->drawLine(P3, P4);
  _editor->drawLine(P4, P1);

  _editor->drawLine(P5, P6);
  _editor->drawLine(P6, P7);
  _editor->drawLine(P7, P8);
  _editor->drawLine(P8, P5);

  _editor->drawLine(P1, P5);
  _editor->drawLine(P2, P6);
  _editor->drawLine(P3, P7);
  _editor->drawLine(P4, P8);

}

inline void
P2::renderScene()
{
  if (auto camera = Camera::current())
  {
    _renderer->setCamera(camera);
    _renderer->setImageSize(width(), height());
    _renderer->setProgram(&_program);
    _renderer->render();
    _program.use();
  }
}

void P2::preview(Camera & camera) {

  //ImGui::Begin("Preview");

  // armazena o tamanho da view port antiga
  int lastViewPort[4];
  glGetIntegerv(GL_VIEWPORT, lastViewPort);

  glViewport(0,0, width()/4, height()/4);

  // impede que os pixels sejam renderizados fora desta �rea
  glEnable(GL_SCISSOR_TEST);
  glScissor(0, 0, width() / 4, height() / 4);
  
  
  _renderer->setCamera(&camera);
  _renderer->setImageSize(width(), height());
  _renderer->setProgram(&_program);
  _renderer->render();
  _program.use();


  glDisable(GL_SCISSOR_TEST);
  glViewport(lastViewPort[0], lastViewPort[1], lastViewPort[2], lastViewPort[3]);

  

  //ImGui::End();

}

constexpr auto CAMERA_RES = 0.01f;
constexpr auto ZOOM_SCALE = 1.01f;

void
P2::render()
{
  if (_viewMode == ViewMode::Renderer)
  {
    renderScene();
    return;
  }
  if (_moveFlags)
  {
    const auto delta = _editor->orbitDistance() * CAMERA_RES;
    auto d = vec3f::null();

    if (_moveFlags.isSet(MoveBits::Forward))
      d.z -= delta;
    if (_moveFlags.isSet(MoveBits::Back))
      d.z += delta;
    if (_moveFlags.isSet(MoveBits::Left))
      d.x -= delta;
    if (_moveFlags.isSet(MoveBits::Right))
      d.x += delta;
    if (_moveFlags.isSet(MoveBits::Up))
      d.y += delta;
    if (_moveFlags.isSet(MoveBits::Down))
      d.y -= delta;
    _editor->pan(d);
  }
  _editor->newFrame();

  // **Begin rendering of temporary scene objects
  // It should be replaced by your rendering code (and moved to scene editor?)
  auto ec = _editor->camera();
  const auto& p = ec->transform()->position();
  auto vp = vpMatrix(ec);

  _program.setUniformMat4("vpMatrix", vp);
  _program.setUniformVec4("ambientLight", _scene->ambientLight);
  _program.setUniformVec3("lightPosition", p);

  for (auto it : _scene->getIteratorPrimitives()) {

    // Pega o objeto do componente
    auto box = it->sceneObject();

    // Caso o primitivo nao esta visivel para a renderiza��o
    if (!box->visible)
      continue;


    if (auto p = dynamic_cast<Primitive*>(it.get()))
      drawPrimitive(*p);
    else if (auto c = dynamic_cast<Camera*>(it.get())) {
      if (box == _current) {
        drawCamera(*c);
      }
     
    }
      
    if (box == _current) {
      auto t = box->transform();
       _editor->drawAxes(t->position(), mat3f{ t->rotation() });
    }
  }

  auto currentBox = dynamic_cast<SceneObject*>(_current);

  // Caso o objeto seja um objecto
  if (currentBox) {
    for (auto it : currentBox->getComponentIterator()) {
      if (auto c = dynamic_cast<Camera*>(it.get())) {
        preview(*c);
      }
    }
  }

}

void
P2::cameraFocus() {

  // Verifica se '_current' eh um objeto 
  if (auto p = dynamic_cast<SceneObject*>(_current)) {

    //Pega a posi��o do objeto
    auto position = p->transform()->position();
    auto scale = p->transform()->localScale();
    auto angleView = convertToRad(_editor->camera()->viewAngle());

    float distance = (scale.y / ( tan(angleView/2))) + (scale.x / ( tan(angleView / 2))) + scale.z;

    // Joga a camera para cima do objeto
    _editor->camera()->transform()->setPosition(position);
    
    // tira a camera de cima do objeto
    vec3f pos = { 0,0, distance };

    _editor->camera()->transform()->translate(pos);

  }

}

bool
P2::windowResizeEvent(int width, int height)
{
  _editor->camera()->setAspectRatio(float(width) / float(height));
  return true;
}

bool
P2::keyInputEvent(int key, int action, int mods)
{
  auto active = action != GLFW_RELEASE && mods == GLFW_MOD_ALT;

  switch (key)
  {
    case GLFW_KEY_W:
      _moveFlags.enable(MoveBits::Forward, active);
      break;
    case GLFW_KEY_S:
      _moveFlags.enable(MoveBits::Back, active);
      break;
    case GLFW_KEY_A:
      _moveFlags.enable(MoveBits::Left, active);
      break;
    case GLFW_KEY_D:
      _moveFlags.enable(MoveBits::Right, active);
      break;
    case GLFW_KEY_Q:
      _moveFlags.enable(MoveBits::Up, active);
      break;
    case GLFW_KEY_E:
      _moveFlags.enable(MoveBits::Down, active);
      break;
    case GLFW_KEY_DELETE:
      if (action == GLFW_RELEASE)
        removeObject();
      break;
    case GLFW_KEY_F:
      if (mods == GLFW_MOD_ALT)
        cameraFocus();
      break;
    case GLFW_KEY_N:
      if (mods == GLFW_MOD_CONTROL && action == GLFW_RELEASE) {
        auto p = dynamic_cast<SceneObject*>(_current);
        if (!p)
          p = _scene->getRoot();
        auto bx = createNode(p, 1, "Box");
      }
      break;
    case GLFW_KEY_M:
      if (mods == GLFW_MOD_CONTROL && action == GLFW_RELEASE) {
        auto p = dynamic_cast<SceneObject*>(_current);
        if (!p)
          p = _scene->getRoot();

        auto bx = createNode(p, 2, "Sphere");
      }
      break;
  }
  return false;
}

bool
P2::scrollEvent(double, double yOffset)
{
  if (ImGui::GetIO().WantCaptureMouse || _viewMode == ViewMode::Renderer)
    return false;
  _editor->zoom(yOffset < 0 ? 1.0f / ZOOM_SCALE : ZOOM_SCALE);
  return true;
}

bool
P2::mouseButtonInputEvent(int button, int actions, int mods)
{
  if (ImGui::GetIO().WantCaptureMouse || _viewMode == ViewMode::Renderer)
    return false;
  (void)mods;

  auto active = actions == GLFW_PRESS;

  if (button == GLFW_MOUSE_BUTTON_RIGHT)
    _dragFlags.enable(DragBits::Rotate, active);
  else if (button == GLFW_MOUSE_BUTTON_MIDDLE)
    _dragFlags.enable(DragBits::Pan, active);
  if (_dragFlags)
    cursorPosition(_pivotX, _pivotY);
  return true;
}

bool
P2::mouseMoveEvent(double xPos, double yPos)
{
  if (!_dragFlags)
    return false;
  _mouseX = (int)xPos;
  _mouseY = (int)yPos;

  const auto dx = (_pivotX - _mouseX);
  const auto dy = (_pivotY - _mouseY);

  _pivotX = _mouseX;
  _pivotY = _mouseY;
  if (dx != 0 || dy != 0)
  {
    if (_dragFlags.isSet(DragBits::Rotate))
    {
      const auto da = -_editor->camera()->viewAngle() * CAMERA_RES;
      isKeyPressed(GLFW_KEY_LEFT_ALT) ?
        _editor->orbit(dy * da, dx * da) :
        _editor->rotateView(dy * da, dx * da);
    }
    if (_dragFlags.isSet(DragBits::Pan))
    {
      const auto dt = -_editor->orbitDistance() * CAMERA_RES;
      _editor->pan(-dt * math::sign(dx), dt * math::sign(dy), 0);
    }
  }
  return true;
}
