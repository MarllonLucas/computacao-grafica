//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: Scene.h
// ========
// Class definition for scene.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 21/09/2019

#ifndef __Scene_h
#define __Scene_h

#include "SceneObject.h"
#include "graphics/Color.h"
#include <list>
#include "Primitive.h"

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// Scene: scene class
// =====
class Scene: public SceneNode
{
public:
  Color backgroundColor{Color::gray};
  Color ambientLight{Color::black};

  /// Constructs an empty scene.
  Scene(const char* name):
    SceneNode{name}
  {
    RootObject = new SceneObject("root", *this);
  }

  ~Scene() {
    clearList();
  }

  auto getRoot()
  {
    return RootObject;
  }

  // Retorna se minha lista de primitivos esta vazia
  bool isEmpty() {
    return components.empty();
  }

  auto size() {
    return components.size();
  }

  // Limpa a minha lista de primitivos
  void clearList() {
    components.clear();
  }

  // Adiciono o primitivo a minha lista de primitivos
  void add(Reference<Component> component) {
    components.push_back(component);
  }
  // Removo o meu primitivo da minha lista de primitivos
  void remove(Reference<Component> component) {
    components.remove(component);
  }

  auto getIteratorPrimitives() {
    return components;
  }

  bool containsComponent(Reference<Component> component) {
    bool contains = false;

    if (!components.empty()){
      for (auto it = components.begin(); it != components.end(); ++it) {
        if (*it == component) {
          contains = true;
          break;
        }
      }
    }

    return contains;
  }

private:

  Reference<SceneObject> RootObject;

  // Lista de objetos que seram renderizados
  std::list<Reference<Component>> components;

}; // Scene

} // end namespace cg

#endif // __Scene_h
