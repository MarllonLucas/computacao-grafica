//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: SceneObject.h
// ========
// Class definition for scene object.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 23/09/2019

#ifndef __SceneObject_h
#define __SceneObject_h

#include "SceneNode.h"
#include "Transform.h"
#include <list>
#include <string.h>
#include <iostream>

namespace cg
{ // begin namespace cg

// Forward definition
class Scene;


/////////////////////////////////////////////////////////////////////
//
// SceneObject: scene object class
// ===========
class SceneObject: public SceneNode
{
public:
  bool visible{true};

  /// Constructs an empty scene object.
  SceneObject(const char* name, Scene& scene):
    SceneNode{name},
    _scene{&scene},
    _parent{}
  {

    add(&_transform);
    makeUse(&_transform);
 
  }

  ~SceneObject();

  /// Returns the scene which this scene object belong to.
  auto scene() const
  {
    return _scene;
  }

  /// Returns the parent of this scene object.
  auto parent() const
  {
    return _parent;
  }

  /// Sets the parent of this scene object.
  void setParent(SceneObject* parent);
  void setParentEditor(SceneObject * parent);
  bool isFather(Reference<SceneObject> object, Reference<SceneObject> reference);

  /// Returns the transform of this scene object.
  auto transform() const
  {
    return &_transform;
  }

  auto transform()
  {
    return &_transform;
  }

  // Retorna se a lista de objetos desse n� esta vazio
  bool getObjectEmpty() {
    return objects_scene.empty();
  }

  auto getObjectsIterator() {
    return objects_scene;
  }

  // Retorna o iterador do inicio do meu objeto
  auto getObjectIteratorBegin() {
    return objects_scene.begin();
  }

  // Retorna o iterador do final do meu objeto
  auto getObjectIteratorEnd() {
    return objects_scene.end();
  }

  // Fa�o sobrecarga no meu metodo
  void add(Reference<SceneObject> object);
  void add(Reference<Component> component);
  void remove(Reference<SceneObject> object);
  void remove(Reference<Component> component);

  bool containsComponent(Reference<Component> component);

  // Retorna a lista de componentes
  auto getComponentIterator() {
    return components;
  }

  // Retorna o iterador do inicio do meu component
  auto getComponentIteratorBegin() {
    std::list<Reference<Component>>::iterator it = components.begin();
    return it;
  }

  // Retorna o iterador do final do meu componente
  auto getComponentIteratorEnd() {
    std::list<Reference<Component>>::iterator it = components.end();
    return it;
  }

  auto getSizeComponents() {
    return components.size();
  }

private:
  Scene* _scene;
  SceneObject* _parent;
  Transform _transform;

   // Create a list of the objects in the scene
  std::list<Reference<SceneObject>> objects_scene;
  std::list<Reference<Component>> components;

  friend class Scene;

}; // SceneObject

/// Returns the transform of a component.
inline Transform*
Component::transform() // declared in Component.h
{
  return sceneObject()->transform();
}

/// Returns the parent of a transform.
inline Transform*
Transform::parent() const // declared in Transform.h
{
  if (auto p = sceneObject()->parent())
    return p->transform();
   return nullptr;
}

} // end namespace cg

#endif // __SceneObject_h
