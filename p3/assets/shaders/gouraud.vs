#version 330 core

// Estrutura Da Luz
struct LightInfo {
	vec4 color;
	vec3 position;
	vec3 direction;
	int falloff;
	int type;
	float angle;
	float spotExponent;	
};
// Estrutura do Material
struct Material {
	vec4 oa;
	vec4 od;
	vec4 os;
	float ns;
};
// Ambiente
uniform vec4 ia;

// Objeto
uniform mat3 normalMatrix;
uniform mat4 transform;

// Camera
uniform mat4 vpMatrix = mat4(1);
uniform vec3 cameraPosition;

// Luz
uniform LightInfo lights[10];
uniform Material material;
uniform int noLights;

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;

out vec4 vertexColor;

vec4 multcolor(vec4 c1, vec4 c2){
	vec4 cout;

	for(int i = 0; i < 4; ++i){
		cout[i] = c1[i] * c2[i];
	}

	return cout;
}

void main()
{
	vec3 Ll;	// direcao da fonte de luz para o ponto iluminado P.  LUZ --------> P
	vec3 RL; // direcao do raio refletido
	float dl;	
	vec4 il;
	float theta;
	float psi;

	// ambient
	vec4 A = ia * material.oa;
	// computa variaveis independentes de i 
	vec4 P = transform * position;
	vec3 V = normalize(vec3(P) - cameraPosition); // direcao em que o observador enxerga o ponto P.			OBS --------> P
	vec3 N = normalize(normalMatrix * normal);    // direcao da normal global
	
	// inicializa variável vertexColor
	vertexColor = A; 
	for (int i = 0; i < noLights ; i++) 
	{		
		if (lights[i].type == 0) // Direcional
		{
			Ll = normalize(lights[i].direction);
			il = lights[i].color;
		}
		else if (lights[i].type == 1) // Pontual
		{
			dl = distance(lights[i].position, vec3(P));
			Ll = normalize(vec3(P) - lights[i].position);
			il = lights[i].color / pow(dl,lights[i].falloff);
		}
		else  // Spot
		{
			dl = distance(lights[i].position, vec3(P));
			Ll = normalize(vec3(P) - lights[i].position);

			theta = radians(lights[i].angle);
			psi = dot(Ll, lights[i].direction);

			il = theta > acos(psi) ? (lights[i].color / pow(dl,lights[i].falloff)) * pow(psi,lights[i].spotExponent) : vec4(0);
		}
		// Calcula RL para computar parte especular
		RL = Ll -2* dot(N,-Ll) * N;

		// Phong Model Formula
		vertexColor += (multcolor(material.od, il) * max(dot(-N, Ll), 0)) + (multcolor(material.os, il) * pow(max(dot(-RL,V),0),material.ns));
	}

 gl_Position = vpMatrix * P;
}