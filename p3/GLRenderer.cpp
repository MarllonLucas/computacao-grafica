//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: GLRenderer.cpp
// ========
// Source file for OpenGL renderer.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 09/09/2019

#include "GLRenderer.h"

namespace cg
{ // begin namespace cg


//////////////////////////////////////////////////////////
//
// GLRenderer implementation
// ==========
void
GLRenderer::update()
{
  Renderer::update();
  // TODO
}

inline void
drawMesh(GLMesh* mesh, GLuint mode)
{
  glPolygonMode(GL_FRONT_AND_BACK, mode);
  glDrawElements(GL_TRIANGLES, mesh->vertexCount(), GL_UNSIGNED_INT, 0);
}

inline void
GLRenderer::drawPrimitive(Primitive& primitive)
{
  auto m = glMesh(primitive.mesh());

  if (nullptr == m)
    return;

  auto t = primitive.transform();
  auto normalMatrix = mat3f{ t->worldToLocalMatrix() }.transposed();

  /* Parametros do material */
  _program->setUniformVec4("material.oa", primitive.material.ambient);
  _program->setUniformVec4("material.od", primitive.material.diffuse);
  _program->setUniformVec4("material.os", primitive.material.spot);
  _program->setUniform("material.ns", primitive.material.shine);

  /* Parametros do objeto */
  _program->setUniformMat3("normalMatrix", normalMatrix);
  _program->setUniformMat4("transform", t->localToWorldMatrix());

  m->bind();

  drawMesh(m, GL_FILL);
}
void
GLRenderer::render()
{
  const auto& bc = _scene->backgroundColor;

  glClearColor(bc.r, bc.g, bc.b, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  auto ec = camera();
  auto vp = vpMatrix(ec);

  _program->setUniformMat4("vpMatrix", vp);

  for (auto it : _scene->getIteratorPrimitives()) {

    // Pega o objeto do componente
    auto box = it->sceneObject();

    // Caso o primitivo nao esta visivel para a renderização
    if (!box->visible)
      continue;

    if (auto p = dynamic_cast<Primitive*>(it.get()))
      drawPrimitive(*p);

  }

}

} // end namespace cg
