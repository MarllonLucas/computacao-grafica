#include "geometry/MeshSweeper.h"
#include "P3.h"
#include <list>

MeshMap P3::_defaultMeshes;

inline void
P3::buildDefaultMeshes()
{
  _defaultMeshes["None"] = nullptr;
  _defaultMeshes["Box"] = GLGraphics3::box();
  _defaultMeshes["Sphere"] = GLGraphics3::sphere();
}

inline Primitive*
makePrimitive(MeshMapIterator mit)
{
  return new Primitive(mit->second, mit->first);
}

inline void
P3::buildScene()
{
  _current = _scene = new Scene{"Scene 1"};
  _editor = new SceneEditor{*_scene};
  _editor->setDefaultView((float)width() / (float)height());
  
  auto camera = createCamera(_scene->getRoot(), "Main Camera");
  auto box    = createNode(_scene->getRoot(), 1, "Box");
  auto bx2    = createNode(box, 1,"Box");
  auto light1 = createLight("Point Light", 0);
  auto light2 = createLight("Directional Light", 1);
  auto light3 = createLight("Spot Light", 2);

  vec3f box1P{ 0, 1.9, 0 };
  vec3f box2P{ 0, -1.2, 0 };
  vec3f box2S{ 2.3, 0.1, 2 };
  vec3f LPP{ 0, 2.4, 3.1 };
  vec3f LDP{ 0,0,4.0 };
  vec3f LDR{ -8.2, 26, 0 };
  vec3f LSP{ 1.1, 2.8, 1.1 };

  box->transform()->setLocalPosition(box1P);
  bx2->transform()->setLocalPosition(box2P);
  bx2->transform()->setLocalScale(box2S);

  light1->transform()->setLocalPosition(LPP);
  light2->transform()->setLocalPosition(LDP);
  light3->transform()->setLocalPosition(LSP);

  light1->color.setRGB(233,80,0);
  light2->color.setRGB(103,45,45);
  light3->color.setRGB(255,255,255);

  Camera::setCurrent(camera);
}

Reference<Camera> 
P3::createCamera(Reference<SceneObject> parent, std::string name) {
  auto parentScene = parent->scene();
  auto object = new SceneObject{ name.c_str(), * parentScene };

  Reference<Camera> camera = new Camera;

  object->add(Reference<Component>(camera));
  object->setParent(parent);

  return camera;
}

Reference<Light>
P3::createLight(std::string name, int typeLight){
  // Pego o objeto que será o pai desta LUZ
  auto parent = dynamic_cast<SceneObject*>(_current) ? dynamic_cast<SceneObject*>(_current) : nullptr; 

  if (!parent)
    parent = dynamic_cast<Scene*>(_current)->getRoot();

  auto parentScene = parent->scene();
  auto object = new SceneObject{ name.c_str(), * parentScene };

  auto type = typeLight == 0 ? Light::Point : (typeLight == 1 ? Light::Directional : Light::Spot);

  Reference<Light> light = new Light(type);

  object->add(Reference<Component>(light));
  object->setParent(parent);

  auto x = parentScene->addLight(light);

  if (!x) {
    std::cout << "Para a dicionar uma outra luz na cena favou desligue ou remova uma luz ativa!" << std::endl;
  }

  return light;
}

Reference<SceneObject>
P3::createNode(Reference<SceneObject> parent, int typeNode, std::string shape) {

  /*
    TypeNode:
      0: Box
      1: Empty Object
      2: Camera
  */

  std::string name = typeNode == 0 ? "Empyt Object " : (typeNode == 1 ? "Box " : "Sphere ");
  // Depois verificar se o objeto que eu quero instaciar como filho seja igual a minha cena passada
  auto parentScene = parent->scene();

  // Pega o nome do meu objeto e adiciona um valor inteiro para o nome 
  name = name + std::to_string(getIndexObjects());

  auto newObject = new SceneObject{ name.c_str(), *parentScene };

  // Verifico se o parent passado é igual a raiz da minha sena
  if (parentScene->getRoot() == parent) newObject->setParent(nullptr);
  else                                  newObject->setParent(parent);

  // Caso o mesmo seja uma box criada
  // Devo adicionar um componente primitivo nele
  if (typeNode) {
    auto primitive = makePrimitive(_defaultMeshes.find(shape));
    newObject->add(Reference<Component>(primitive));
  }

  return newObject;

}

void
P3::initialize()
{
  Application::loadShaders(_programGouraud, "shaders/gouraud.vs", "shaders/gouraud.fs");
  Application::loadShaders(_programPhong,   "shaders/phong.vs",   "shaders/phong.fs");
  
  Assets::initialize();
  buildDefaultMeshes();
  buildScene();
  _renderer = new GLRenderer{*_scene};
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(1.0f, 1.0f);
  glEnable(GL_LINE_SMOOTH);
  
  GLSL::Program::setCurrent(&_programPhong);

}

void
P3::hierarchyWindowRecursive(Reference<SceneObject> node) {
  bool openNode = false;
  ImGuiTreeNodeFlags flag{ ImGuiTreeNodeFlags_OpenOnArrow };

  if (node != node->scene()->getRoot()) {

    // Verifica se eu estou em um nó intermediario
    if (!node->getObjectEmpty()) {
      openNode = ImGui::TreeNodeEx(node, _current == node ? flag | ImGuiTreeNodeFlags_Selected : flag, node->name());
    }
    else {
      flag |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
      ImGui::TreeNodeEx(node, _current == node ? flag | ImGuiTreeNodeFlags_Selected : flag, node->name());
    }

    if (ImGui::IsItemClicked()) _current = node;

    if (ImGui::BeginDragDropSource()) {

      ImGui::Text(node->name());
      ImGui::SetDragDropPayload("SceneObject", &node, sizeof(node));
      ImGui::EndDragDropSource();
    
    }

    if (ImGui::BeginDragDropTarget()) {

      if (auto* payload = ImGui::AcceptDragDropPayload("SceneObject")) {

        // Pega o objeto enviado
        auto obj = *(cg::Reference<SceneObject>*)payload->Data;

        bool isFather = obj->isFather(node, obj);

        if (isFather) {
          // Faz o ajuste de parametros
          obj->setParent(node);
        }
        else {
          std::cout << "Nao foi possivel fazer a alteracao" << std::endl;
        }
      
      }

      ImGui::EndDragDropTarget();
    }

  }

  // Verifico se o meu no esta aberto na minha interface, ou se o mesmo é a minha raiz
  if (openNode || _scene->getRoot() == node) {

    for (auto it : node->getObjectsIterator())
      hierarchyWindowRecursive(it);

    ImGui::TreePop();

  }

}

inline void
P3::removeObject() {
  if (auto object = dynamic_cast<SceneObject*>(_current)) {

    auto father = object->parent();
    father->remove(object);

    if (father == father->scene()->getRoot())
      _current = father->scene();
    else
      _current = father;
  }
}

inline void
P3::hierarchyWindow()
{
  ImGui::Begin("Hierarchy");
  if (ImGui::Button("Create###object"))
    ImGui::OpenPopup("CreateObjectPopup");
  if (ImGui::BeginPopup("CreateObjectPopup"))
  {

    auto emptyObj = dynamic_cast<SceneObject*>(_current);

    // Begin add empty object
    if (ImGui::MenuItem("Empty Object")){
      auto newEmpty = emptyObj ? createNode(emptyObj, 0, "None") : createNode(_scene->getRoot(), 0, "None");
    } // End add empty object

    // Begin add 3D object
    if (ImGui::BeginMenu("3D Object")){
      
      if (ImGui::MenuItem("Box"))
        auto newObj = emptyObj ? createNode(emptyObj, 1, "Box") : createNode(_scene->getRoot(), 1, "Box");
      if (ImGui::MenuItem("Sphere"))
        auto newObj = emptyObj ? createNode(emptyObj, 2, "Sphere") : createNode(_scene->getRoot(), 2, "Sphere");
      
      ImGui::EndMenu();
    
    } // End add 3D Object

    // Begin add light
    if (ImGui::BeginMenu("Light"))
    {

      if (ImGui::MenuItem("Directional Light")) {
        std::string name = "Directional Light " + std::to_string(getIndexObjects());
        auto newLight = createLight(name, 1);
      }

      if (ImGui::MenuItem("Point Light")) {
        std::string name = "Point Light " + std::to_string(getIndexObjects());
        auto newLight = createLight(name, 0);
      }

      if (ImGui::MenuItem("Spotlight")) {
        std::string name = "Spot Light " + std::to_string(getIndexObjects());
        auto newLight = createLight(name, 2);
      }

      ImGui::EndMenu();
    } // End add light

    // Begin add Camera
    if (ImGui::MenuItem("Camera")){
      std::string name = "Camera " + std::to_string(getIndexObjects());
      auto camera = emptyObj ? createCamera(emptyObj, name.c_str()) : createCamera(_scene->getRoot(), name.c_str());
    } // End add Camera

    ImGui::EndPopup();
  }
  

  ImGui::SameLine();
  if (ImGui::Button("Delete"))
    removeObject();

  ImGui::Separator();

  ImGuiTreeNodeFlags flag{ ImGuiTreeNodeFlags_OpenOnArrow };
  auto open = ImGui::TreeNodeEx(_scene, _current == _scene ? flag | ImGuiTreeNodeFlags_Selected : flag, _scene->name());

  if (ImGui::IsItemClicked())
    _current = _scene;

  if (ImGui::BeginDragDropTarget()) {
    if (auto* payload = ImGui::AcceptDragDropPayload("SceneObject")) {
      // Pega o objeto enviado
      auto obj = *(cg::Reference<SceneObject>*)payload->Data;
      obj->setParent(_scene->getRoot());
    }

    ImGui::EndDragDropTarget();
  }

  if (open) {
    // TODO: fazer uma verificação para uma cena
    hierarchyWindowRecursive(_scene->getRoot());
  }

  ImGui::End();
}

namespace ImGui
{ // begin namespace ImGui

void
ObjectNameInput(NameableObject* object)
{
  const int bufferSize{128};
  static NameableObject* current;
  static char buffer[bufferSize];

  if (object != current)
  {
    strcpy_s(buffer, bufferSize, object->name());
    current = object;
  }
  if (ImGui::InputText("Name", buffer, bufferSize))
    object->setName(buffer);
}

inline bool
ColorEdit3(const char* label, Color& color)
{
  return ImGui::ColorEdit3(label, (float*)&color);
}

inline bool
DragVec3(const char* label, vec3f& v)
{
  return DragFloat3(label, (float*)&v, 0.1f, 0.0f, 0.0f, "%.2g");
}

void
TransformEdit(Transform* transform)
{
  vec3f temp;

  temp = transform->localPosition();
  if (ImGui::DragVec3("Position", temp))
    transform->setLocalPosition(temp);
  temp = transform->localEulerAngles();
  if (ImGui::DragVec3("Rotation", temp))
    transform->setLocalEulerAngles(temp);
  temp = transform->localScale();
  if (ImGui::DragVec3("Scale", temp)){

    if (temp.x < 0.001)
      temp.x = 0.001;
    if (temp.y < 0.001)
      temp.y = 0.001;
    if (temp.y < 0.001)
      temp.y = 0.001;

    transform->setLocalScale(temp);
  }
}

} // end namespace ImGui

inline void
P3::sceneGui()
{
  auto scene = (Scene*)_current;

  ImGui::ObjectNameInput(_current);
  ImGui::Separator();
  if (ImGui::CollapsingHeader("Colors"))
  {
    ImGui::ColorEdit3("Background", scene->backgroundColor);
    ImGui::ColorEdit3("Ambient Light", scene->ambientLight);
  }
}

inline void
P3::inspectShape(Primitive& primitive)
{
  char buffer[16];

  snprintf(buffer, 16, "%s", primitive.meshName());
  ImGui::InputText("Mesh", buffer, 16, ImGuiInputTextFlags_ReadOnly);
  if (ImGui::BeginDragDropTarget())
  {
    if (auto* payload = ImGui::AcceptDragDropPayload("PrimitiveMesh"))
    {
      auto mit = *(MeshMapIterator*)payload->Data;
      primitive.setMesh(mit->second, mit->first);
    }
    ImGui::EndDragDropTarget();
  }
  ImGui::SameLine();
  if (ImGui::Button("...###PrimitiveMesh"))
    ImGui::OpenPopup("PrimitiveMeshPopup");
  if (ImGui::BeginPopup("PrimitiveMeshPopup"))
  {
    auto& meshes = Assets::meshes();

    if (!meshes.empty())
    {
      for (auto mit = meshes.begin(); mit != meshes.end(); ++mit)
        if (ImGui::Selectable(mit->first.c_str()))
          primitive.setMesh(Assets::loadMesh(mit), mit->first);
      ImGui::Separator();
    }
    for (auto mit = _defaultMeshes.begin(); mit != _defaultMeshes.end(); ++mit)
      if (ImGui::Selectable(mit->first.c_str()))
        primitive.setMesh(mit->second, mit->first);
    ImGui::EndPopup();
  }
}

inline void
P3::inspectMaterial(Material& material)
{
  ImGui::ColorEdit3("Ambient", material.ambient);
  ImGui::ColorEdit3("Diffuse", material.diffuse);
  ImGui::ColorEdit3("Spot", material.spot);
  ImGui::DragFloat("Shine", &material.shine, 1, 0, 1000.0f);
}

inline void
P3::inspectPrimitive(Primitive& primitive)
{
    inspectShape(primitive);
    inspectMaterial(primitive.material);
}

inline void
P3::inspectLight(Light& light)
{
  static const char* lightTypes[]{"Directional", "Point", "Spot"};
  auto lt = light.type();

  if (ImGui::BeginCombo("Type", lightTypes[lt]))
  {
    for (auto i = 0; i < IM_ARRAYSIZE(lightTypes); ++i){
      bool selected = lt == i;

      if (ImGui::Selectable(lightTypes[i], selected))
        lt = (Light::Type)i;
      if (selected)
        ImGui::SetItemDefaultFocus();
    }

    ImGui::EndCombo();
  }
  light.setType(lt);
  ImGui::ColorEdit3("Color", light.color);

  if (light.type() == Light::Spot)
  {
    auto lf = light.falloff();
    if (ImGui::SliderInt("Falloff", &lf, 0, 2, "%.0f"))
      light.setFalloff(lf);
    auto la = light.angle();
    if (ImGui::SliderFloat("Opening Angle", &la, 0, 90.0f, "%.0f deg", 1.0f))
      light.setAngle(la);
    auto spotExp = light.spotExponent();
    if (ImGui::SliderInt("Spot Exponent", &spotExp, 0, 2, "%.0f"))
      light.setSpotExponent(spotExp);
  }
  else if (light.type() == Light::Point)
  {
    auto lf = light.falloff();
    if (ImGui::SliderInt("Falloff", &lf, 0, 2, "%.0f"))
      light.setFalloff(lf);
  }
}

void
P3::inspectCamera(Camera& camera)
{
  static const char* projectionNames[]{"Perspective", "Orthographic"};
  auto cp = camera.projectionType();

  if (ImGui::BeginCombo("Projection", projectionNames[cp])){
    for (auto i = 0; i < IM_ARRAYSIZE(projectionNames); ++i){
      auto selected = cp == i;

      if (ImGui::Selectable(projectionNames[i], selected))
        cp = (Camera::ProjectionType)i;
      if (selected)
        ImGui::SetItemDefaultFocus();
    }
    ImGui::EndCombo();
  }
  camera.setProjectionType(cp);
  if (cp == View3::Perspective)
  {
    auto fov = camera.viewAngle();

    if (ImGui::SliderFloat("View Angle",
      &fov,
      MIN_ANGLE,
      MAX_ANGLE,
      "%.0f deg",
      1.0f))
      camera.setViewAngle(fov <= MIN_ANGLE ? MIN_ANGLE : fov);
  }
  else
  {
    auto h = camera.height();

    if (ImGui::DragFloat("Height",
      &h,
      MIN_HEIGHT * 10.0f,
      MIN_HEIGHT,
      math::Limits<float>::inf()))
      camera.setHeight(h <= 0 ? MIN_HEIGHT : h);
  }

  float n;
  float f;

  camera.clippingPlanes(n, f);

  if (ImGui::DragFloatRange2("Clipping Planes",
    &n,
    &f,
    MIN_DEPTH,
    MIN_DEPTH,
    math::Limits<float>::inf(),
    "Near: %.2f",
    "Far: %.2f"))
  {
    if (n <= 0)
      n = MIN_DEPTH;
    if (f - n < MIN_DEPTH)
      f = n + MIN_DEPTH;
    camera.setClippingPlanes(n, f);
  }
}

inline void
P3::addComponentButton(SceneObject& object)
{
  if (ImGui::Button("Add Component"))
    ImGui::OpenPopup("AddComponentPopup");
  if (ImGui::BeginPopup("AddComponentPopup"))
  {
    if (ImGui::MenuItem("Primitive"))
    {   
      // caso eu possa adicionar um primitivo no meu objeto
      if (!object.primitive()) {
        auto primitive = makePrimitive(_defaultMeshes.find("Box"));
        object.add(Reference<Component>(primitive));
      }
      else {
        std::cout << "Nao foi possivel adicionar um primitivo" << std::endl;
      }

    }
    if (ImGui::MenuItem("Light"))
    {
      if(!object.light()) {
        // Cria uma luz pontual por padrao
        Reference<Light> light = new Light(Light::Point);
        object.add(Reference<Component>(light));

        // Adiciono a minha luz na cena
        bool x = object.scene()->addLight(light);

        if (!x) {
          std::cout << "Não é possivel adicionar mais que " << NL << " luzes na cena, desligue uma outra luz e depois acenda esta." << std::endl;
        }

      }else{
        std::cout << "Nao foi eh possivel adicionar mais de 1 luz no mesmo objeto" << std::endl;
      }
    }
    if (ImGui::MenuItem("Camera"))
    {
      if (!object.camera()) {

        Reference<Camera> camera = new Camera;
        object.add(Reference<Component>(camera));

      }
      else {

        std::cout << "Nao foi possivel adicionar uma camera" << std::endl;

      }
    }
    ImGui::EndPopup();
  }
}

inline void
P3::sceneObjectGui()
{
  auto object = (SceneObject*)_current;

  addComponentButton(*object);
  ImGui::Separator();
  ImGui::ObjectNameInput(object);
  ImGui::SameLine();
  ImGui::Checkbox("###visible", &object->visible);
  ImGui::Separator();
  if (ImGui::CollapsingHeader(object->transform()->typeName()))
    ImGui::TransformEdit(object->transform());
  

  for (auto it : object->getComponentIterator()){

    // Caso o componente atual seja do tipo primitivo
    if (auto p = dynamic_cast<Primitive*>(it.get())) {

      auto notDelete{ true };
      auto open = ImGui::CollapsingHeader(p->typeName(), &notDelete);

      if (!notDelete) {
        object->remove(Reference<Component>(p));
      }
      else if (open)
        inspectPrimitive(*p);
    }

    else if (auto c = dynamic_cast<Camera*>(it.get())) {

      auto notDelete{ true };
      auto open = ImGui::CollapsingHeader(c->typeName(), &notDelete);

      if (!notDelete) {
        object->remove(Reference<Component>(c));
      }
      else if (open)
      {
        auto isCurrent = c == Camera::current();

        ImGui::Checkbox("Current", &isCurrent);
        Camera::setCurrent(isCurrent ? c : nullptr);
        inspectCamera(*c);
      }

    }

    else if (auto l = dynamic_cast<Light*>(it.get())){
      
      auto notDelete{true};
      auto open = ImGui::CollapsingHeader(l->typeName(), &notDelete);

      if (!notDelete){
        object->remove(Reference<Component>(l));
      }
      else if (open) {
        // Verifico se a minha luz esta visivel
        if (l->isTurnedOn()) {
          if (ImGui::Button("Turn Off")) {
            // Retiro a minha luz da minha renderização da scene
            object->scene()->removeLight(l);
          }
        }

        else if (!l->isTurnedOn()) {
          if (ImGui::Button("Turn On")) {
            auto x = object->scene()->addLight(l);
            if (!x) {
              ImGui::OpenPopup("Error TurnOn");
            }
          }

          bool openM = true;
          if (ImGui::BeginPopupModal("Error TurnOn", &openM)) {
            ImGui::Text("Desligue uma outra luz para poder ligar esta luz");
            if (ImGui::Button("Close"))
              ImGui::CloseCurrentPopup();
            ImGui::EndPopup();
          }
        }
        else
          ImGui::MenuItem("TurnOff another light so you can TurnOn this one");

        inspectLight(*l);
      }
    }
  }

}

inline void
P3::objectGui()
{
  if (_current == nullptr)
    return;
  if (dynamic_cast<SceneObject*>(_current))
  {
    sceneObjectGui();
    return;
  }
  if (dynamic_cast<Scene*>(_current))
    sceneGui();
}

inline void
P3::inspectorWindow()
{
  ImGui::Begin("Inspector");
  objectGui();
  ImGui::End();
}

inline void
P3::editorViewGui()
{
  if (ImGui::Button("Set Default View"))
    _editor->setDefaultView(float(width()) / float(height()));
  ImGui::Separator();

  auto t = _editor->camera()->transform();
  vec3f temp;

  temp = t->localPosition();
  if (ImGui::DragVec3("Position", temp))
    t->setLocalPosition(temp);
  temp = t->localEulerAngles();
  if (ImGui::DragVec3("Rotation", temp))
    t->setLocalEulerAngles(temp);
  inspectCamera(*_editor->camera());
  ImGui::Separator();
  
  // Função para alterar o shader atual de coloração dos primitivos
  renderModeGui();
  
  ImGui::Separator();
  ImGui::Checkbox("Show Ground", &_editor->showGround);
}

inline void
P3::assetsWindow()
{
  ImGui::Begin("Assets");
  if (ImGui::CollapsingHeader("Meshes"))
  {
    auto& meshes = Assets::meshes();

    for (auto mit = meshes.begin(); mit != meshes.end(); ++mit)
    {
      auto meshName = mit->first.c_str();
      auto selected = false;

      ImGui::Selectable(meshName, &selected);
      if (ImGui::BeginDragDropSource())
      {
        Assets::loadMesh(mit);
        ImGui::Text(meshName);
        ImGui::SetDragDropPayload("PrimitiveMesh", &mit, sizeof(mit));
        ImGui::EndDragDropSource();
      }
    }
  }
  ImGui::Separator();
  if (ImGui::CollapsingHeader("Textures"))
  {
    // next semester
  }
  ImGui::End();
}

inline void
P3::editorView()
{
  if (!_showEditorView)
    return;
  ImGui::Begin("Editor View Settings");
  editorViewGui();
  ImGui::End();
}

inline void
P3::fileMenu()
{
  if (ImGui::MenuItem("New"))
  {
    // TODO
  }
  if (ImGui::MenuItem("Open...", "Ctrl+O"))
  {
    // TODO
  }
  ImGui::Separator();
  if (ImGui::MenuItem("Save", "Ctrl+S"))
  {
    // TODO
  }
  if (ImGui::MenuItem("Save As..."))
  {
    // TODO
  }
  ImGui::Separator();
  if (ImGui::MenuItem("Exit", "Alt+F4"))
  {
    shutdown();
  }
}

inline bool
showStyleSelector(const char* label)
{
  static int style = 1;

  if (!ImGui::Combo(label, &style, "Classic\0Dark\0Light\0"))
    return false;
  switch (style)
  {
    case 0: ImGui::StyleColorsClassic();
      break;
    case 1: ImGui::StyleColorsDark();
      break;
    case 2: ImGui::StyleColorsLight();
      break;
  }
  return true;
}

inline void
P3::showOptions()
{
  ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.6f);
  showStyleSelector("Color Theme##Selector");
  ImGui::ColorEdit3("Selected Wireframe", _selectedWireframeColor);
  ImGui::PopItemWidth();
}

inline void
P3::mainMenu()
{
  if (ImGui::BeginMainMenuBar())
  {
    if (ImGui::BeginMenu("File"))
    {
      fileMenu();
      ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("View"))
    {
      if (Camera::current() == 0)
        ImGui::MenuItem("Edit View", nullptr, true, false);
      else
      {
        static const char* viewLabels[]{"Editor", "Renderer"};

        if (ImGui::BeginCombo("View", viewLabels[_viewMode]))
        {
          for (auto i = 0; i < IM_ARRAYSIZE(viewLabels); ++i)
          {
            if (ImGui::Selectable(viewLabels[i], _viewMode == i))
              _viewMode = (ViewMode)i;
          }
          ImGui::EndCombo();
        }
      }
      ImGui::Separator();
      ImGui::MenuItem("Assets Window", nullptr, &_showAssets);
      ImGui::MenuItem("Editor View Settings", nullptr, &_showEditorView);
      ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Tools"))
    {
      if (ImGui::BeginMenu("Options"))
      {
        showOptions();
        ImGui::EndMenu();
      }
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
}

void
P3::gui()
{
  mainMenu();
  hierarchyWindow();
  inspectorWindow();
  assetsWindow();
  editorView();
}

inline void
drawMesh(GLMesh* mesh, GLuint mode)
{
  glPolygonMode(GL_FRONT_AND_BACK, mode);
  glDrawElements(GL_TRIANGLES, mesh->vertexCount(), GL_UNSIGNED_INT, 0);
}

inline void
P3::drawPrimitive(Primitive& primitive)
{
  
  auto m = glMesh(primitive.mesh());
  if (nullptr == m)
    return;

  auto t = primitive.transform();
  auto normalMatrix = mat3f{t->worldToLocalMatrix()}.transposed();

  /* Parametros do material */
  GLSL::Program::current()->setUniformVec4("material.oa", primitive.material.ambient);
  GLSL::Program::current()->setUniformVec4("material.od", primitive.material.diffuse);
  GLSL::Program::current()->setUniformVec4("material.os", primitive.material.spot);
  GLSL::Program::current()->setUniform("material.ns",     primitive.material.shine);

  /* Parametros do objeto */
  GLSL::Program::current()->setUniformMat3("normalMatrix", normalMatrix);
  GLSL::Program::current()->setUniformMat4("transform", t->localToWorldMatrix());

  m->bind();
  
  drawMesh(m, GL_FILL);

  if (primitive.sceneObject() != _current)
    return;

  if (_showEdges) {
    GLSL::Program::current()->setUniformVec4("material.oa", _edgeColor); // oa
    drawMesh(m, GL_LINE);
  }

}

inline void
P3::renderModeGui()
{
  int index = GLSL::Program::current() == &_programGouraud ? 0 : 1;
  
  static const char* shadingTypes[]{ "Gouraud", "Phong" };
  static const char* item_current = shadingTypes[index];
  if (ImGui::BeginCombo("Shading Mode", item_current))
  {
    for (auto i = 0; i < IM_ARRAYSIZE(shadingTypes); ++i)
    {
      bool selected = item_current == shadingTypes[i];
      if (ImGui::Selectable(shadingTypes[i], selected)) {
        item_current = shadingTypes[i];
        switch (i){
        case(0):
          GLSL::Program::setCurrent(&_programGouraud);
          break;
        case(1):
          GLSL::Program::setCurrent(&_programPhong);
          break;
        }
      }
      if (selected)
        ImGui::SetItemDefaultFocus();
    }
    ImGui::EndCombo();
  }

  ImGui::ColorEdit3("Edges", _edgeColor);
  ImGui::SameLine();
  ImGui::Checkbox("###showEdges", &_showEdges);
}

inline void
P3::drawLight(Light& light)
{
  auto t = light.transform();
  // Vetor normal
  auto foward = t->rotation() * vec3f{0,0,-1};
  auto up = t->up();
  // Posição
  auto position = t->position();
  auto rotation = t->rotation();

  _editor->setLineColor(light.color);

  // Caso a luz que ira ser exibida é do tipo pontual
  if (light.type() == Light::Type::Point) {
    vec3f P1, P2, P3, P4, P5, P6, P7, P8;
    // Seta todos os valores para o ponto "(0,0,0)" do sistema local
    P1 = P2 = P3 = P4 = P5 = P6 = P7 = P8 = position;

    P1[0] += 0.2f;
    P1[1] += 0.2f;
    P2[0] -= 0.2f;
    P2[1] -= 0.2f;
    P3[1] += 0.3f;
    P4[1] -= 0.3f;
    P5[0] -= 0.2f;
    P5[1] += 0.2f;
    P6[0] += 0.2f;
    P6[1] -= 0.2f;
    P7[0] += 0.3f;
    P8[0] -= 0.3f;


    // faço o desenho de uma estrela com apenas linhas

    _editor->drawLine(P1, P4);
    _editor->drawLine(P1, P8);
    _editor->drawLine(P2, P3);
    _editor->drawLine(P2, P7);
    _editor->drawLine(P3, P6);
    _editor->drawLine(P4, P5);
    _editor->drawLine(P5, P7);
    _editor->drawLine(P6, P8);
  }
  else if (light.type() == Light::Type::Directional) {

    _editor->drawVector((position + up * 0.2f), foward, 1);
    _editor->drawVector( position, foward, 1);
    _editor->drawVector((position - up * 0.2f), foward, 1);

  }
  else if (light.type() == Light::Type::Spot) {

    auto cPostion = rotation * vec3f(0, 0, 1) + position;
    auto cRadius = light.angle() / 90.0;
    _editor->drawCircle(position, cRadius, foward);

  }

}

inline float
convertToRad(float theta) {
  return theta*(M_PI/180);
}

inline void
P3::drawCamera(Camera& camera)
{
  float F, B, viewAngle, H_min, H_max;
  auto BF = camera.clippingPlanes(F, B);

  B = B * 0.7;

  auto M = mat4f{ camera.cameraToWorldMatrix() };

  if (camera.projectionType() == Camera::ProjectionType::Perspective) {
    viewAngle = convertToRad(camera.viewAngle());

    H_min = 2 * F * tanf(viewAngle / 2);
    H_max = 2 * B * tanf(viewAngle / 2);
  }
  else 
    H_min = H_max = camera.height();

  vec3f P1, P2, P3, P4, P5, P6, P7, P8;

  P1 = M.transform(vec3f{ H_min / 2 ,  H_min / 2, -F });
  P2 = M.transform(vec3f{-H_min / 2 ,  H_min / 2, -F });
  P3 = M.transform(vec3f{-H_min / 2 , -H_min / 2, -F });
  P4 = M.transform(vec3f{ H_min / 2 , -H_min / 2, -F });

  P5 = M.transform(vec3f{ H_max / 2 ,  H_max / 2, -B });
  P6 = M.transform(vec3f{-H_max / 2 ,  H_max / 2, -B });
  P7 = M.transform(vec3f{-H_max / 2 , -H_max / 2, -B });
  P8 = M.transform(vec3f{ H_max / 2 , -H_max / 2, -B });

  _editor->setLineColor(Color::black);

  _editor->drawLine(P1, P2);
  _editor->drawLine(P2, P3);
  _editor->drawLine(P3, P4);
  _editor->drawLine(P4, P1);

  _editor->drawLine(P5, P6);
  _editor->drawLine(P6, P7);
  _editor->drawLine(P7, P8);
  _editor->drawLine(P8, P5);

  _editor->drawLine(P1, P5);
  _editor->drawLine(P2, P6);
  _editor->drawLine(P3, P7);
  _editor->drawLine(P4, P8);

}

inline void
P3::renderScene()
{
  if (auto camera = Camera::current()){

    selectedLights();
    startLightsAndCamera();

    _renderer->setCamera(camera);
    _renderer->setImageSize(width(), height());
    _renderer->setProgram(&_programPhong);
    _renderer->render();
  }
}

constexpr auto CAMERA_RES = 0.01f;
constexpr auto ZOOM_SCALE = 1.01f;

void P3::preview(Camera & camera) {

  // armazena o tamanho da view port antiga
  int lastViewPort[4];
  glGetIntegerv(GL_VIEWPORT, lastViewPort);

  glViewport(0,0, width()/4, height()/4);

  // impede que os pixels sejam renderizados fora desta área
  glEnable(GL_SCISSOR_TEST);
  glScissor(0, 0, width() / 4, height() / 4);
  
  _renderer->setCamera(&camera);
  _renderer->setImageSize(width(), height());
  _renderer->setProgram(GLSL::Program::current());
  _renderer->render();
  GLSL::Program::current()->use();

  glDisable(GL_SCISSOR_TEST);
  glViewport(lastViewPort[0], lastViewPort[1], lastViewPort[2], lastViewPort[3]);

}

void
P3::selectedLights()
{
  GLSL::Program::current()->setUniform("noLights", _scene->sizeLight);
  for (int i = 0, j = 0; i < NL; i++){
    if (_scene->light[i] != nullptr) {
      auto name = "lights[" + std::to_string(j) + "].";
      
      GLSL::Program::current()->setUniformVec4((name + "color").c_str(), _scene->light[i]->color); // cor
      GLSL::Program::current()->setUniform((name + "type").c_str(), _scene->light[i]->type()); // type

      GLSL::Program::current()->setUniformVec3((name + "position").c_str(), _scene->light[i]->position()); // position
      GLSL::Program::current()->setUniformVec3((name + "direction").c_str(), _scene->light[i]->rotation() * vec3f { 0, 0, -1 }); // direction

      GLSL::Program::current()->setUniform((name + "falloff").c_str(), _scene->light[i]->falloff()); // falloff
      GLSL::Program::current()->setUniform((name + "angle").c_str(), _scene->light[i]->angle()); // angle
      GLSL::Program::current()->setUniform((name + "spotExponent").c_str(), _scene->light[i]->spotExponent()); // spotExponent																																																		
     
      j++; // Incrementa o contador da luz
    }
  }
}

void
P3::startLightsAndCamera(){
  auto camera = _editor->camera();
  GLSL::Program::current()->setUniformVec4("ia", _scene->ambientLight);
  GLSL::Program::current()->setUniformVec3("cameraPosition", camera->transform()->position());
  GLSL::Program::current()->setUniformMat4("vpMatrix", vpMatrix(camera));
}

void
P3::drawPrimitives() {

  for (auto it : _scene->getIteratorPrimitives()) {
    // Pega o objeto do componente
    auto box = it->sceneObject();

    // Caso o primitivo nao esta visivel para a renderização
    if (!box->visible)
      continue;

    if (auto p = dynamic_cast<Primitive*>(it.get()))
      drawPrimitive(*p);
    else if (auto c = dynamic_cast<Camera*>(it.get())) {
      if (box == _current) {
        drawCamera(*c);
      }
    }

    if (box == _current) {
      auto t = box->transform();
      _editor->drawAxes(t->position(), mat3f{ t->rotation() });
    }
  }

  auto currentBox = dynamic_cast<SceneObject*>(_current);

  // Caso o objeto seja um objecto
  if (currentBox) {
    for (auto it : currentBox->getComponentIterator()) {
      if (auto c = dynamic_cast<Camera*>(it.get())) {
        preview(*c);
      }
    }
  }

  // Passo pelas minhas luzes
  for (int i = 0; i < NL; ++i) {
    if (_scene->light[i] != nullptr) {
      drawLight(*(_scene->light[i]));
    }
  }

}

void
P3::render()
{
  if (_viewMode == ViewMode::Renderer)
  {
    renderScene();
    return;
  }
  if (_moveFlags)
  {
    const auto delta = _editor->orbitDistance() * CAMERA_RES;
    auto d = vec3f::null();

    if (_moveFlags.isSet(MoveBits::Forward))
      d.z -= delta;
    if (_moveFlags.isSet(MoveBits::Back))
      d.z += delta;
    if (_moveFlags.isSet(MoveBits::Left))
      d.x -= delta;
    if (_moveFlags.isSet(MoveBits::Right))
      d.x += delta;
    if (_moveFlags.isSet(MoveBits::Up))
      d.y += delta;
    if (_moveFlags.isSet(MoveBits::Down))
      d.y -= delta;
    _editor->pan(d);
  }
  _editor->newFrame();

  selectedLights();
  startLightsAndCamera();
  drawPrimitives();

}

void
P3::cameraFocus() {

  // Verifica se '_current' eh um objeto 
  if (auto p = dynamic_cast<SceneObject*>(_current)) {

    //Pega a posição do objeto
    auto position = p->transform()->position();
    auto scale = p->transform()->localScale();
    auto angleView = convertToRad(_editor->camera()->viewAngle());

    float distance = (scale.y / ( tan(angleView/2))) + (scale.x / ( tan(angleView / 2))) + scale.z;

    // Joga a camera para cima do objeto
    _editor->camera()->transform()->setPosition(position);
    
    // tira a camera de cima do objeto
    vec3f pos = { 0,0, distance };

    _editor->camera()->transform()->translate(pos);
  }

}

bool
P3::windowResizeEvent(int width, int height)
{
  _editor->camera()->setAspectRatio(float(width) / float(height));
  return true;
}

bool
P3::keyInputEvent(int key, int action, int mods)
{
  auto active = action != GLFW_RELEASE && mods == GLFW_MOD_ALT;

  switch (key)
  {
    case GLFW_KEY_W:
      _moveFlags.enable(MoveBits::Forward, active);
      break;
    case GLFW_KEY_S:
      _moveFlags.enable(MoveBits::Back, active);
      break;
    case GLFW_KEY_A:
      _moveFlags.enable(MoveBits::Left, active);
      break;
    case GLFW_KEY_D:
      _moveFlags.enable(MoveBits::Right, active);
      break;
    case GLFW_KEY_Q:
      _moveFlags.enable(MoveBits::Up, active);
      break;
    case GLFW_KEY_Z:
      _moveFlags.enable(MoveBits::Down, active);
      break;
    case GLFW_KEY_DELETE:
      if (action == GLFW_RELEASE)
        removeObject();
      break;
    case GLFW_KEY_F:
      if (mods == GLFW_MOD_ALT)
        cameraFocus();
      break;
    case GLFW_KEY_N:
      if (mods == GLFW_MOD_CONTROL && action == GLFW_RELEASE) {
        auto p = dynamic_cast<SceneObject*>(_current);
        if (!p)
          p = _scene->getRoot();
        auto bx = createNode(p, 1, "Box");
      }
      break;
    case GLFW_KEY_M:
      if (mods == GLFW_MOD_CONTROL && action == GLFW_RELEASE) {
        auto p = dynamic_cast<SceneObject*>(_current);
        if (!p)
          p = _scene->getRoot();

        auto bx = createNode(p, 2, "Sphere");
      }
      break;
  }
  return false;
}

bool
P3::scrollEvent(double, double yOffset)
{
  if (ImGui::GetIO().WantCaptureMouse)
    return false;
  _editor->zoom(yOffset < 0 ? 1.0f / ZOOM_SCALE : ZOOM_SCALE);
  return true;
}

bool
P3::mouseButtonInputEvent(int button, int actions, int mods)
{
  if (ImGui::GetIO().WantCaptureMouse)
    return false;
  (void)mods;

  auto active = actions == GLFW_PRESS;

  if (button == GLFW_MOUSE_BUTTON_RIGHT)
    _dragFlags.enable(DragBits::Rotate, active);
  else if (button == GLFW_MOUSE_BUTTON_MIDDLE)
    _dragFlags.enable(DragBits::Pan, active);
  if (_dragFlags)
    cursorPosition(_pivotX, _pivotY);
  return true;
}

bool
P3::mouseMoveEvent(double xPos, double yPos)
{
  if (!_dragFlags)
    return false;
  _mouseX = (int)xPos;
  _mouseY = (int)yPos;

  const auto dx = (_pivotX - _mouseX);
  const auto dy = (_pivotY - _mouseY);

  _pivotX = _mouseX;
  _pivotY = _mouseY;
  if (dx != 0 || dy != 0)
  {
    if (_dragFlags.isSet(DragBits::Rotate))
    {
      const auto da = -_editor->camera()->viewAngle() * CAMERA_RES;
      isKeyPressed(GLFW_KEY_LEFT_ALT) ?
        _editor->orbit(dy * da, dx * da) :
        _editor->rotateView(dy * da, dx * da);
    }
    if (_dragFlags.isSet(DragBits::Pan))
    {
      const auto dt = -_editor->orbitDistance() * CAMERA_RES;
      _editor->pan(-dt * math::sign(dx), dt * math::sign(dy), 0);
    }
  }
  return true;
}
